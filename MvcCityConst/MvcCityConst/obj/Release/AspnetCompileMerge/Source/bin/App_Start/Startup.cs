﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using CC = MvcCityConst.Common;
using System.Security.Principal;

[assembly: OwinStartup(typeof(MvcCityConst.Startup))]

namespace MvcCityConst
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            CC.InitTimers();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
               AuthenticationType= "ApplicationCookie",
                LoginPath = new PathString("/Account/Login"),
                 
                Provider = new CookieAuthenticationProvider
                {
                     
                    OnValidateIdentity = context =>
                    {
                        return Task.Factory.StartNew(() =>
                        {

                            if (context.Identity.IsAuthenticated)
                            {
                                var db = CC.SystemModel;
                                string ID = context.Identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                                var User = db.Users.FirstOrDefault(fod =>(fod.State==1 || fod.State==3)&& fod.ID.ToString() == ID);
                                if (User == null || context.Identity.FindFirst("SecurityStamp").Value != User.SecurityStamp.ToString())
                                {

                                    //  Account.LogOut();


                                    foreach (var cookie in context.Request.Cookies)
                                    {
                                        context.Response.Cookies.Append(cookie.Key, cookie.Value, new CookieOptions { Expires = DateTime.Now.AddDays(-1) });
                                    }
                                    context.Response.Redirect("\\");
                                }
                                else
                                {
                                   // Account.SetUpAdmin();
                                }
                            }
                            
                        });
                    }
                }
            });
        }
    }
}