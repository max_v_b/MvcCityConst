//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcCityConst.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SysUserСпКонтрагенты
    {
        public int IDSysUserСпКонтрагенты { get; set; }
        public Nullable<int> IDКонтрагенты { get; set; }
        public Nullable<int> IDОбуМестоПоложение { get; set; }
        public Nullable<int> IDОбуМестоПоложениеТек { get; set; }
        public string SysUser { get; set; }
        public Nullable<int> TypeSysUser { get; set; }
        public string CurUser { get; set; }
        public Nullable<System.DateTime> CurDateTime { get; set; }
    
        public virtual СпКонтрагенты СпКонтрагенты { get; set; }
        public virtual СпОбуМестоПоложение СпОбуМестоПоложение { get; set; }
    }
}
