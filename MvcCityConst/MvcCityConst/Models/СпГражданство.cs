//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcCityConst.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class СпГражданство
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public СпГражданство()
        {
            this.СпРабочие = new HashSet<СпРабочие>();
        }
    
        public int IDГражданство { get; set; }
        public string Гражданство { get; set; }
        public string CurUser { get; set; }
        public Nullable<System.DateTime> CurDateTime { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СпРабочие> СпРабочие { get; set; }
    }
}
