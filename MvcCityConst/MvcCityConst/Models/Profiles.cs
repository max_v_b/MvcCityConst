//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcCityConst.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Profiles
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Profiles()
        {
            this.Accounts = new HashSet<Accounts>();
            this.Contacts = new HashSet<Contacts>();
            this.Employees = new HashSet<Employees>();
            this.Hostels = new HashSet<Hostels>();
            this.Organizations = new HashSet<Organizations>();
            this.Subscriptions = new HashSet<Subscriptions>();
            this.MessageRecepients = new HashSet<MessageRecepients>();
            this.Messages = new HashSet<Messages>();
        }
    
        public int Nx { get; set; }
        public System.Guid ID { get; set; }
        public Nullable<System.Guid> Users_ID { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public Nullable<int> Gender_Nx { get; set; }
        public string INN { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string Passport { get; set; }
        public string Adress { get; set; }
        public string Comments { get; set; }
        public int State { get; set; }
        public System.DateTime DI { get; set; }
        public Nullable<System.Guid> UI { get; set; }
        public Nullable<System.DateTime> DM { get; set; }
        public Nullable<System.Guid> UM { get; set; }
        public int ProfileType_Nx { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Accounts> Accounts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contacts> Contacts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employees> Employees { get; set; }
        public virtual Gender Gender { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hostels> Hostels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organizations> Organizations { get; set; }
        public virtual ProfileType ProfileType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subscriptions> Subscriptions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MessageRecepients> MessageRecepients { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Messages> Messages { get; set; }
    }
}
