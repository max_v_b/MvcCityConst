﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Employees
{
    public class Employees
    {
        public int AddEvent(int EmployeeEvents_Nx, int Employee_Nx, DateTime? DateBegin=null, DateTime? DateEnd = null, string Comments="")
        {
            int ret = 0;
            var db = CC.CityConstDB;
           var Empl= db.EmployeesHistory.Add(new Models.EmployeesHistory
            {
                EmployeeEvents_Nx = EmployeeEvents_Nx,
                Employees_Nx = Employee_Nx,
                DateBegin = DateBegin ?? DateTime.Now,
                DateEnd = DateEnd,
                Comments = Comments,
                DI = DateTime.Now,
                UI = Account.GetCurrentUserID()
            });
            db.SaveChanges();
            ret = Empl.Nx;
            return ret;
        }
    }
}