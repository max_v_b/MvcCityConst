﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.Mvc;
using System.Web.WebSockets;

namespace MvcCityConst
{
    public class Common
    {
        public struct WebSocketClient { public WebSocket Socket; public Guid UserID; }
        public static List<WebSocketClient> WebClients = new List<WebSocketClient>();
        public static Models.CityConstEntities CityConstModel { get { return new MvcCityConst.Models.CityConstEntities(); } }
        public static Models.CityConstSystemEntities SystemModel { get { return new Models.CityConstSystemEntities(); } }
        public static Models.CityConstDBEntities CityConstDB { get { return new Models.CityConstDBEntities(); } }
        public static int? ContrID { get { return (int?)HttpContext.Current.Session["ContrID"]; } }

        public static string NormalizeName(string Name)
        {
            Name = Name ?? "";
            var ReplSimb = " ~`!@#$%^&*()_+=-{}[];:\"'\\|/.,".ToList();
            ReplSimb.ForEach(f => Name=Name.Replace(f.ToString(), ""));
            return Name.ToLower();
        }
        public delegate void Method(dynamic Data);
        public struct EventAndData { public Method SubMethod; public dynamic Data; }
        private static Dictionary<string,Timer> Timers=new Dictionary<string, Timer>();
        private static Dictionary<string, List<EventAndData>> Subscriptions = new Dictionary<string, List<EventAndData>>();
        public static void InitTimers()
        {
            var db = CityConstDB;
            db.SubscriptionInterval.Where(wh => wh.Interval > 0).ToList().ForEach(item => {

                if(Timers["interval"+item.Interval.ToString()]==null)
                {
                    Timer tmr = new Timer();
                    tmr.Interval = item.Interval * 60 * 1000;
                    tmr.Elapsed += Tmr_Elapsed;
                    tmr.Enabled = true;
                    Timers.Add("interval" + item.Interval.ToString(), tmr);
                }

            });
        }

        private static void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            string Key = "interval"+((Timer)sender).Interval.ToString();

            var sub = Subscriptions[Key];
            if (sub != null)
            {
                sub.ForEach(item =>
                {
                    item.SubMethod(item.Data);
                });
            }
            Subscriptions.Remove(Key);
        }
     
        public static bool SubscribeOnEvent(EventAndData EAD, Models.SubscriptionInterval SI)
        {
            bool ret = false;
            string Key = "interval" + SI.Interval.ToString();
            if (Timers[Key] != null)
            {
                var sub = Subscriptions[Key];
                if (sub == null)
                {
                    Subscriptions.Add(Key, new List<EventAndData>());
                    sub = Subscriptions[Key];
                }
                sub.Add(EAD);
                ret = true;
            }
            return ret;
        }


        //private  Task WebSocketRequest(AspNetWebSocketContext context)
        //{
           
        //    return Task.
        //    var socket = context.WebSocket;

        //    try
        //    {
        //        Clients.Add(socket);
        //    }
        //    catch
        //    {

        //    }


        //}

        }
        public class OptionalData { public string targetName { get; set; } public string Values { get; set; } public string valuesName { get; set; } public int ViewID { get; set; } }

   
}