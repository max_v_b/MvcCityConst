﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConfirmationToken = MvcCityConst.Models.Tokens;
using CC = MvcCityConst.Common;
using System.Threading.Tasks;

namespace MvcCityConst
{
    public class ConfirmationTokens
    {

        public enum ConfirmationTokenState : int { Failed = 0, Awaiting, Confirmed, Expired, Canceled };
        public static ConfirmationToken Issue(string Subject, int LifeTime, int SystemEvent_Nx)
        {
            var sb = CC.SystemModel;
            ConfirmationToken ct = new ConfirmationToken() { ID = Guid.NewGuid(), Issued = DateTime.Now, LifeTime = LifeTime, Code = (new Random()).Next(100000, 999999), Comment = Subject, State = (int)ConfirmationTokenState.Awaiting, RemainingTries = 3, SystemEvents_Nx= SystemEvent_Nx };
            sb.Tokens.Add(ct);
            sb.SaveChanges();
            return ct;
        }
        public static ConfirmationToken Issue(string Subject, int SystemEvent_Nx)
        {
            return Issue(Subject, 259200/*3 days*/, SystemEvent_Nx);
        }
        public static Task<ConfirmationTokenState> ConfirmAsync(Guid ID, string Code)
        {
            return Task.Factory.StartNew<ConfirmationTokenState>(() =>
            {
                var sb = CC.SystemModel;
                var ct = sb.Tokens.FirstOrDefault(cc => cc.ID == ID);
                ConfirmationTokenState cts = ConfirmationTokenState.Failed;
                if (ct != null && ct.State == (int)ConfirmationTokenState.Awaiting)
                {

                    if ((ct.Issued.AddSeconds(ct.LifeTime) <= DateTime.Now))
                        ct.State = (int)ConfirmationTokenState.Expired;
                    else
                    {
                        ct.RemainingTries--;
                        if (ct.Code == int.Parse(Code))
                            ct.State = (int)ConfirmationTokenState.Confirmed;

                        else if (ct.RemainingTries > 0)
                        {
                            ct.State = (int)ConfirmationTokenState.Awaiting;

                        }
                        else
                            ct.State = (int)ConfirmationTokenState.Canceled;
                    }
                    cts = ((ConfirmationTokenState)ct.State) == ConfirmationTokenState.Awaiting ? ConfirmationTokenState.Failed : (ConfirmationTokenState)ct.State;
                    sb.SaveChanges();
                }

                return cts;
            });
        }
        public static Task<ConfirmationToken> GetAsync(Guid ID)
        {
            return Task.Factory.StartNew(() =>
            {
                return CC.SystemModel.Tokens.FirstOrDefault(cc => cc.ID == ID);
            });

        }
        public static Task<ConfirmationTokenState> GetStateAsync(Guid ID)
        {
            return Task.Factory.StartNew<ConfirmationTokenState>(() =>
            {
                ConfirmationTokenState cts = ConfirmationTokenState.Failed;
                var sb = CC.SystemModel;
                var ct = sb.Tokens.FirstOrDefault(cc => cc.ID == ID);
                if (ct != null)
                    if (ct.Issued.AddSeconds(ct.LifeTime) <= DateTime.Now)
                    {
                        ct.State = (int)ConfirmationTokenState.Expired;
                        sb.SaveChanges();
                    }
                cts = (ConfirmationTokenState)ct.State;
                return cts;
            });
        }
        public static ConfirmationTokenState GetState(Guid ID)
        {

            ConfirmationTokenState cts = ConfirmationTokenState.Failed;
            var sb = CC.SystemModel;
            var ct = sb.Tokens.FirstOrDefault(cc => cc.ID == ID);
            if (ct != null)
                if (ct.Issued.AddSeconds(ct.LifeTime) <= DateTime.Now)
                {
                    ct.State = (int)ConfirmationTokenState.Expired;
                    sb.SaveChanges();
                }
            cts = (ConfirmationTokenState)ct.State;
            return cts;

        }
        public static void Renew(Guid ID)
        {
            Task.Factory.StartNew(() =>
            {
                var sb = CC.SystemModel;
                var ct = sb.Tokens.FirstOrDefault(cc => cc.ID == ID);
                if (ct != null)
                {
                    ct.Issued = DateTime.Now;
                    ct.State = (int)ConfirmationTokenState.Awaiting;
                    ct.Code = (new Random()).Next(100000, 999999);
                    sb.SaveChanges();
                }
            });
        }
        public static void Cancel(Guid ID)
        {
            Task.Factory.StartNew(() =>
            {
                var sb = CC.SystemModel;
                var ct = sb.Tokens.FirstOrDefault(cc => cc.ID == ID);
                if (ct != null)
                {
                    ct.State = (int)ConfirmationTokenState.Canceled;
                    sb.SaveChanges();
                }
            });
        }
    }
}