﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MvcCityConst
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start( )
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Account.SetUpAdmin();

        }
       
        //protected void Application_BeginRequest(Object source, EventArgs e)
        //{
           
        //}
        //protected void Application_AcquireRequestState(Object sender, EventArgs e)
        //{

        //}
        //protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        //{
        //}
        //protected void Application_PostRequestHandlerExecute(Object sender, EventArgs e)
        //{

        //}
        //protected void Application_ReleaseRequestState(Object sender, EventArgs e)
        //{

        //}
        //protected void Application_UpdateRequestCache(Object sender, EventArgs e)
        //{

        //}

        //protected void Application_EndRequest(Object sender, EventArgs e)
        //{

          

        //}
        //protected void Application_PreSendRequestContent(Object sender, EventArgs e)
        //{
        //}

    }

}
