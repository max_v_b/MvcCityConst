﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Owin.Security;
using Microsoft.Web.WebSockets;
using MvcCityConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebSockets;
using CC = MvcCityConst.Common;
namespace MvcCityConst
{
    public class Account
    {
        private static IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.Request.GetOwinContext().Authentication;
            }
        }
        public static List<Models.Profiles> ProfilesCache { get { return (List<Models.Profiles>)HttpContext.Current.Application["ProfilesCach"]; } set { HttpContext.Current.Application["ProfilesCach"] = value; } }
        public static List<Models.Users> UsersCache { get { return (List<Models.Users>)HttpContext.Current.Application["UsersCach"]; } set { HttpContext.Current.Application["UsersCach"] = value; } }
        public static Guid SystemProfileID { get { return new Guid("B82D4CE1-CEA0-41C1-AF96-F60AFE57A423"); } }
        public static Guid? GetCurrentUserID()
        { 
            Guid? ret = null;
            if (HttpContext.Current.User!= null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Guid outg;
                if (Guid.TryParse(((ClaimsIdentity)HttpContext.Current.User.Identity).FindFirst(ClaimTypes.NameIdentifier).Value, out outg))
                    ret = outg;
            }
            return ret;
        }
        public static string GetUsersNotifContact(Guid User_ID)
        {
            //var db = CC.CityConstDB;
            return ProfilesCache.Single(s => s.Users_ID == User_ID).Contacts.Single(s => s.State > 0 && s.Priority == 0).Value;
        }
        public static int? GetCurrentUserState()
        {
            int? ret = null;
           if(HttpContext.Current.Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
            {
                ret = int.Parse(HttpContext.Current.Request.GetOwinContext().Authentication.User.FindFirst("UserState").Value);
            }
            return ret;
        }
        public static int? GetCurrentOrgState()
        {
            int? ret = null;
            if (HttpContext.Current.Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
            {
                var User_ID = Account.GetCurrentUserID();
                var contr = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User_ID)).Employees.Single().Organizations;
                ret = contr.State;
            }
            return ret;
        }
        public static Models.Organizations GetCurrentOrg()
        {
            Models.Organizations ret = null;
            if (HttpContext.Current.Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
            {
                var User_ID = Account.GetCurrentUserID();
                var Org = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User_ID)).Employees.Single().Organizations;
                ret = Org;
            }
            return ret;
        }
        public static void AddSystemLog(int SystemEventNx, string TextInfo, Guid? User_ID = null)
        {
            var db = CC.SystemModel;
            if (db.SystemEvents.Any(any => any.Nx == SystemEventNx))
            {
                db.SystemLogs.Add(new Models.SystemLogs { SystemEvents_Nx = SystemEventNx, TextInfo = TextInfo, User_ID = User_ID, DI = DateTime.Now, UI = GetCurrentUserID() });
                db.SaveChanges();
            }
            else
                throw new Exception("Incorrect SystemEventNx. It doesn't exist in DataBase.");
        }

        public static bool ShouldToChangePass(string UserName)
        {
            var User = Account.GetUser(UserName);
            return Account.VerifyHashedPassword(User.PasswordHash, User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss"));
                
        }

        public static bool ChangeOldPassword(Guid UserID, string OldPassword, string NewPassword)
        {
            bool ret = false;

            var sb = CC.SystemModel;
            var User = sb.Users.FirstOrDefault(fod => fod.ID == UserID);
            //var User = db.Users.Find(UserID);
            if (User != null)
            {
                if (VerifyHashedPassword(User.PasswordHash, OldPassword))
                {
                    User.PasswordHash = HashPassword(NewPassword);
                    sb.SaveChanges();
                    ChangeSecurityStamp(User.ID);
                    Account.CheckSystemCache(true);
                    ret = true;
                }
            }

            return ret;
        }

        public static void ChangeSecurityStamp(Guid UserID)
        {

            var db = CC.SystemModel;
            var User = db.Users.FirstOrDefault(fod => fod.ID == UserID);
            //var User = db.Users.Find(UserID);
            if (User != null)
            {
                User.SecurityStamp = Guid.NewGuid();
                db.SaveChanges();
                if (CC.WebClients.Count > 0)
                {
                    // var WSUser = CC.WebClients.SingleOrDefault(s => s.WebSocketContext.User.Identity.Name == User.UserName);
                    var WSUser = CC.WebClients.SingleOrDefault(s => s.UserID == User.ID);
                   // if (WSUser!=null)
                       Send(WSUser.Socket,"REFRESH");
                }
            }
        }

        public static int? GetUserProfile_Nx(Guid Users_ID)
        {
            int? ret = null;
            //var db = CC.CityConstDB;

            //var Profile=db.Profiles.FirstOrDefault(fod => fod.Users_ID == Users_ID);
            var Profile = ProfilesCache.FirstOrDefault(fod => fod.Users_ID == Users_ID);
            if (Profile != null)
                ret = Profile.Nx;

            return ret;
        }

        public static Models.Users GetUser(int Profiles_Nx)
        {
            var db = CC.CityConstDB;
            //var User = GetUser(db.Profiles.Single(s => s.Nx == Profiles_Nx && s.ProfileType_Nx == 1).Users_ID.Value);
            //var User = GetUser(db.Profiles.Find(Profiles_Nx).Users_ID.Value);
            var User = GetUser(ProfilesCache.First(f=>f.Nx==Profiles_Nx).Users_ID.Value);
            return User;
        }

        public static Models.Users GetUser(Guid UserID)
        {
            var db = CC.SystemModel;
            //var User = db.Users.FirstOrDefault(fod => fod.ID == UserID);
            //var User = db.Users.Find(UserID);
            var User =UsersCache.FirstOrDefault(fod => fod.ID == UserID);
            return User;
        }
        public static Models.Users GetUser(string UserName)
        {
            var db = CC.SystemModel;
            // var User = db.Users.FirstOrDefault(fod => fod.UserName.ToLower() == UserName.ToLower());
            var User =UsersCache.SingleOrDefault(fod => fod.UserName.ToLower() == UserName.ToLower() && fod.State>0);
            return User;
        }

        public static bool LogIn(string UserName, string Password, bool RememberMe = true)
        {
            bool ret = false;
            var db = CC.SystemModel;
            var User = db.Users.FirstOrDefault(fod => (fod.State == 1 || fod.State == 3/*Незаполненный профиль*/) && fod.UserName.ToLower() == UserName.ToLower());
            if (User != null && VerifyHashedPassword(User.PasswordHash, Password))
            {
                ret = true;
                LogIn(User, RememberMe);

            }
            AddSystemLog(ret ? 6 : 7, UserName);
            return ret;
        }
        private static void LogIn(Models.Users User, bool RememberMe)
        {

            if (User != null)
            {
                ClaimsIdentity claim = new ClaimsIdentity("ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                claim.AddClaim(new Claim(ClaimTypes.NameIdentifier, User.ID.ToString(), ClaimValueTypes.String));
                claim.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, User.UserName, ClaimValueTypes.String));
                claim.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                    "OWIN Provider", ClaimValueTypes.String));
                claim.AddClaim(new Claim("SecurityStamp", User.SecurityStamp.ToString()));
                claim.AddClaim(new Claim("UserState", User.State.ToString()));
                if (User.UsersInRoles.Where(wh => wh.State == 1).Count() > 0)
                    User.UsersInRoles.Where(wh => wh.State == 1).ToList().ForEach(role =>
                    {
                        claim.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, role.Roles.RoleName, ClaimValueTypes.String));
                    });
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = RememberMe,
                    ExpiresUtc = DateTimeOffset.MaxValue
                }, claim);

            }

        }

        public static void LogOut()
        {
            AuthenticationManager.SignOut();
        }
        protected static void CheckSystemProfile()
        {
            CheckSystemCache();
            var db = CC.CityConstDB;
            Guid SystemProfileID = Account.SystemProfileID;

            if (!ProfilesCache.Any(any => any.ID == SystemProfileID) && !db.Profiles.Any(any => any.ID == SystemProfileID))
            {
                db.Profiles.Add(new Models.Profiles { ID = SystemProfileID, FirstName = "System", Comments = "Системный профиль. Создан автоматически. Не удалять.", DI = DateTime.Now, ProfileType_Nx = 1, State = 1, Gender_Nx = 2 });
                db.SaveChanges();
            }
            if (ProfilesCache.FirstOrDefault(fod => fod.Organizations.SingleOrDefault(s => s.OrgType_Nx == 1 && s.State > 1) != null) != null) {

                var Org = db.Organizations.Single(s => s.State > 1 & s.OrgType_Nx == 1);

                Org.State = 1;
                db.SaveChanges();
            }
        }
        public static void CheckSystemCache(bool IsForceUpdateCache=false)
        {
            if (HttpContext.Current != null)
            {
                if (IsForceUpdateCache)
                {
                    UsersCache = null;
                    ProfilesCache = null;
                }
                bool rez = false;
                if (UsersCache == null)
                {
                    var sb = CC.SystemModel;
                    UsersCache = sb.Users.ToList();
                    rez = true;
                }
                if (ProfilesCache == null)
                {
                    var db = CC.CityConstDB;
                    ProfilesCache = db.Profiles.ToList();
                    rez = true;
                }
                if (rez)
                    AddSystemLog(16, "", GetCurrentUserID());
            }
            
        }
        public static void SetUpAdmin()
        {
            CheckSystemProfile();
            string userName = System.Configuration.ConfigurationManager.AppSettings["Sys.Admin"];
            bool AddNewEmployee = false;
            if (!string.IsNullOrEmpty(userName))
            {
                var usr = UsersCache.SingleOrDefault(fod => fod.UserName.ToLower() == userName.ToLower());

                if (!string.IsNullOrEmpty(userName) && (usr == null || usr.State != 1))
                {
                    var sb = CC.SystemModel;
                    Models.Roles role;
                    if (!sb.Roles.Any(any => any.RoleName == "Client.Admin" && any.State == 1))
                    {
                        sb.Roles.Add(new Models.Roles { RoleName = "Client.Admin", DI = DateTime.Now, ID = Guid.NewGuid(), State = 1 });
                    }
                    if (!sb.Roles.Any(any => any.RoleName == "Sys.Admin" && any.State == 1))
                    {
                        role = sb.Roles.Add(new Models.Roles { RoleName = "Sys.Admin", DI = DateTime.Now, ID = Guid.NewGuid(), State = 1 });
                    }
                    else
                        role = sb.Roles.First(any => any.RoleName == "Sys.Admin" && any.State == 1);
                    var User = sb.Users.FirstOrDefault(fod => fod.UserName.ToLower() == userName.ToLower());
                    string UserPassword = "";
                    if (User == null)
                    {
                        User = sb.Users.Add(new Models.Users { ID = Guid.NewGuid(), DI = DateTime.Now, SecurityStamp = Guid.NewGuid(), State = 1, UserName = userName });
                        UserPassword = User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss");
                        User.PasswordHash = HashPassword(UserPassword);
                        User.UsersInRoles.Add(new Models.UsersInRoles { Roles = role, Users = User, State = 1 });
                        Account.AddSystemLog(10, User.UserName);
                        Account.AddSystemLog(4, "пользователь: " + User.UserName + " роль: " + role.RoleName);
                        AddNewEmployee = true;
                        CheckSystemCache(true);
                    }
                    else if (User.State != 1)
                    {
                        User.State = 1;
                        UserPassword = User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss");
                        User.PasswordHash = HashPassword(UserPassword);
                        User.UM = User.ID;
                        User.DM = DateTime.Now;
                        if (!User.UsersInRoles.Any(any => any.Roles.RoleName == "Sys.Admin" && any.State == 1))
                        {
                            User.UsersInRoles.Add(new Models.UsersInRoles { Roles = role, Users = User, State = 1 });
                            Account.AddSystemLog(4, "пользователь: " + User.UserName + " роль: " + role.RoleName);
                        }
                        var db = CC.CityConstDB;
                        if (db.Employees.Single(s => s.Profiles.Users_ID == User.ID).State == 0)
                            AddNewEmployee = true;
                        CheckSystemCache(true);
                    }

                    sb.SaveChanges();

                    if (AddNewEmployee)
                    {
                        var db = CC.CityConstDB;
                        var Org = db.Organizations.Single(s => s.State > 0 & s.OrgType_Nx == 1);
                        var Token = ConfirmationTokens.Issue("Подтверждение email автоматически созданного Администратора системы. См. Web.Config=>appSettings=>key='Sys.Admin'", 10);
                        var profile = db.Profiles.Add(new Models.Profiles { ID = Guid.NewGuid(), DI = DateTime.Now, UI = User.ID, Users_ID = User.ID, State = 1, ProfileType_Nx = 1, FirstName = "", Comments = "Профиль автоматически создан системой с правами Администратора системы." });
                        profile.Contacts.Add(new Models.Contacts { Priority = 0, Tokens_ID = Token.ID, ContactType_Nx = 1, State = 2, ID = Guid.NewGuid(), DI = DateTime.Now, UI = User.ID, Value = User.UserName, Comments = "Контакт автоматически создан системой при регистрации пользователя." });
                        db.Employees.Add(new Models.Employees { Chief = 0, Organization_Nx = Org.Nx, DI = DateTime.Now, UI = User.ID, State = 1, Profiles = profile });
                        db.SaveChanges();
                        CheckSystemCache(true);
                        string href = string.Format("http://{0}/Account/Confirmation/{1}?Code={2}", HttpContext.Current.Request.Url.Authority, Token.ID.ToString(), Token.Code.ToString());
                        string Body = string.Format("Ваша учетная запись будет разблокирована после подтверждения адреса Email, указанного при создании пользователя.<br/>Ваше имя пользователя: {0}<br/> пароль:{2}<br/>После первого входа в Систему, пароль требуется изменить.<br/>Для подтверждения Email и активации учетной записи перейдите по <a href='{1}'>ссылке</a>", User.UserName, href, UserPassword);
                        Messages.Mail.SendMail(User.UserName, "Добро пожаловть на корпоративный портал", Body);
                        Messages.Mail.SendMessage(db.Profiles.Single(s => s.ID == SystemProfileID).Nx, 2, "Добро пожаловть на корпоративный портал", "Вы зарегистрированы, как Администратор системы. Вы можете назначать других Администраторов системы, добавлять, удалять, блокировать и просматривать всех пользователей системы. Не забудьте корректно заполнить свой профайл и профайл Вашей орагнизации.", new System.Collections.Generic.List<int> { profile.Nx });
                    }
                }
            }
        }

        public static int RegisterNewEmployee(int Organization_Nx, string Email, string FirstName, string SecondName, string ThirdName, string Gender)
        {
            int ret = 0;
            var db = CC.CityConstDB;
            var sb = CC.SystemModel;
            var CheckUser = Account.GetUser(Email);
            if (!db.Contacts.Any(any => any.Value.ToLower() == Email.ToLower() && any.State != 0 && any.Priority == 0 && any.ContactType_Nx == 1) && !string.IsNullOrEmpty(FirstName) && db.Organizations.Any(any => any.State != 0 && any.Nx == Organization_Nx) && CheckUser == null)
            {
                var CurUser = Account.GetCurrentUserID();
                var User = sb.Users.Add(new Models.Users { ID = Guid.NewGuid(), DI = DateTime.Now,  SecurityStamp = Guid.NewGuid(), State = 2, UserName = Email, UI= CurUser });
                string UserPassword = User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss");
                User.PasswordHash = HashPassword(UserPassword);
                sb.SaveChanges();
                Account.AddSystemLog(GetCurrentUserID()==null?11:1, User.UserName);

                var Token = ConfirmationTokens.Issue("Подтверждение Email вновь созданного пользователя.", 2);
                var profile = db.Profiles.Add(new Models.Profiles { ID = Guid.NewGuid(), DI = DateTime.Now, UI = CurUser ?? User.ID, Users_ID = User.ID, State = 1, ProfileType_Nx = 1, FirstName = FirstName, SecondName = SecondName, ThirdName = ThirdName, Gender_Nx=Gender.ToLower()=="male"?1:2, Comments = "Регистрация пользователя по запросу" });
                profile.Contacts.Add(new Models.Contacts { Priority = 0, Tokens_ID = Token.ID, ContactType_Nx = 1, State = 2, ID = Guid.NewGuid(), DI = DateTime.Now, UI = CurUser ?? User.ID, Value = User.UserName, Comments = "Контакт автоматически создан системой при регистрации пользователя." });
                profile.Employees.Add(new Models.Employees { Chief = 0, Organization_Nx = Organization_Nx, DI = DateTime.Now, UI = CurUser ?? User.ID, State = 1 });
                db.SaveChanges();
                ret = profile.Nx;
                var Org = db.Organizations.Single(any => any.State != 0 && any.Nx == Organization_Nx);
                string href = string.Format("http://{0}/Account/Confirmation/{1}?Code={2}", HttpContext.Current.Request.Url.Authority, Token.ID.ToString(), Token.Code.ToString());
                string Body = string.Format("Ваша учетная запись будет разблокирована после подтверждения адреса Email, указанного при создании пользователя.<br/>Ваше имя пользователя: {0}<br/> пароль:{2}<br/>После первого входа в Систему, пароль требуется изменить.<br/>Для подтверждения Email и активации учетной записи перейдите по <a href='{1}'>ссылке</a>", User.UserName, href, UserPassword);
                Messages.Mail.SendMail(User.UserName, "Добро пожаловть на корпоративный портал", Body);
                Messages.Mail.SendMessage(db.Profiles.Single(s => s.ID == SystemProfileID).Nx, 2, "Добро пожаловть на корпоративный портал", string.Format("Вы зарегистрированы, как пользователь системы от оранизации {0}. Не забудьте корректно заполнить свой профайл. Некорректно заполненный профайл может привести к отказу в активации или блокировке Вашего аккаунта", Org.Name + ", " + Org.BusinessLegalForms.Name), new System.Collections.Generic.List<int> { profile.Nx });
                Account.CheckSystemCache(true);
            }
            return ret;
        }

        public static bool CheckOrgName(string OrgName)
        {
            var db = CC.CityConstDB;
            var NormOrgName = CC.NormalizeName(OrgName);
            return db.Organizations.Where(wh => wh.State != 0).ToList().Any(any => CC.NormalizeName(any.Name) == NormOrgName); ;
        }

        public static int RegisterNewOrganization(string OrgName, int cbBusinessLegalFormsValue, string OrgEmail)
        {
            int ret = 0;
            var db = CC.CityConstDB;
            var sb = CC.SystemModel;
            if (!string.IsNullOrEmpty(OrgName) && cbBusinessLegalFormsValue>0 && !string.IsNullOrEmpty(OrgEmail) && !CheckOrgName(OrgName) && !db.Contacts.Any(any => any.ContactType_Nx == 1 && any.Value.ToLower() == OrgEmail.ToLower() && any.State == 1 && any.Priority == 0))
            {
                var CurUser = Account.GetCurrentUserID();
                var Org = db.Organizations.Add(new Models.Organizations { BusinessLegalForms_Nx = cbBusinessLegalFormsValue,UI= CurUser, DI = DateTime.Now, EmployeesMaxNumber = 100, Name = OrgName, OrgType_Nx = 2, State = 3, Profiles = new Models.Profiles { ID=Guid.NewGuid(), ProfileType_Nx = 2, FirstName = OrgName, DI = DateTime.Now , UI= CurUser } });
               
                var User = sb.Users.Add(new Models.Users { DI = DateTime.Now, ID = Guid.NewGuid(), SecurityStamp = Guid.NewGuid(), State = 2, UserName = OrgEmail, UI= CurUser });
                string UserPassword = User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss");
                User.PasswordHash = HashPassword(UserPassword);
                var role = sb.Roles.Single(s => s.RoleName == "Client.Admin");
                User.UsersInRoles.Add(new Models.UsersInRoles { Users = User, Roles = role, State = 1 });
                sb.SaveChanges();
                //db.Dispose();
                //db = CC.CityConstDB;
                //Org = db.Organizations.Single(s => s.Nx == Org.Nx && s.State!=0);

                Account.AddSystemLog(CurUser == null ? 14 : 1, Org.Name + ", " + db.BusinessLegalForms.Single(s => s.Nx == Org.BusinessLegalForms_Nx).Name + "; Администратор: " + User.UserName, User.ID);


                var Token = ConfirmationTokens.Issue("Подтверждение Email вновь созданного администратора клиента и орагнизации.", 14);
                var profile = db.Profiles.Add(new Models.Profiles { ID = Guid.NewGuid(), DI = DateTime.Now, UI = CurUser ?? User.ID, Users_ID = User.ID, State = 1, ProfileType_Nx = 1, FirstName = "", Comments = "Регистрация пользователя по запросу на регистрацию организации и администратора клиента." });
                profile.Contacts.Add(new Models.Contacts { Priority = 0, Tokens_ID = Token.ID, ContactType_Nx = 1, State = 2, ID = Guid.NewGuid(), DI = DateTime.Now, UI = CurUser ?? User.ID, Value = User.UserName, Comments = "Контакт автоматически создан системой при регистрации пользователя." });
                profile.Employees.Add(new Models.Employees { Chief = 0, Organizations = Org, DI = DateTime.Now, UI = CurUser ?? User.ID, State = 1 });
                db.SaveChanges();


                string href = string.Format("http://{0}/Account/Confirmation/{1}?Code={2}", HttpContext.Current.Request.Url.Authority, Token.ID.ToString(), Token.Code.ToString());
                string Body = string.Format("Ваша учетная запись будет разблокирована после подтверждения адреса Email, указанного при создании пользователя.<br/>Ваше имя пользователя: {0}<br/> пароль:{2}<br/>После первого входа в Систему, пароль требуется изменить.<br/>Для подтверждения Email и активации учетной записи перейдите по <a href='{1}'>ссылке</a>", User.UserName, href, UserPassword);
                Messages.Mail.SendMail(User.UserName, "Добро пожаловть на корпоративный портал", Body);
                Messages.Mail.SendMessage(db.Profiles.Single(s => s.ID == SystemProfileID).Nx, 2, "Добро пожаловть на корпоративный портал", string.Format("Вы зарегистрированы, как Администратор клиента системы от оранизации {0}. Не забудьте корректно заполнить свой профайл и профайл Вашей организации. Некорректно заполненный профайл может привести к отказу в активации или блокировке Вашего аккаунта и аккаунта Вашей организации. Вам будут приходить уведомления о регистрации в системе Ваших сотруднников, Вы, так же, должны контролировать корректность заполнения их профайлов.", Org.Name + ", " + Org.BusinessLegalForms.Name), new System.Collections.Generic.List<int> { profile.Nx });
                ret = Org.Profiles.Nx;
                Account.CheckSystemCache(true);
            }

            return ret;
        }
        public static bool ReIssueToken(Guid id, string Reason="")
        {
            var db = CC.CityConstDB;
            // var sb = CC.SystemModel;
            var contact = db.Contacts.FirstOrDefault(fod => fod.Tokens_ID == id);
            var Token = ConfirmationTokens.GetAsync(id).Result;
            bool ret = false;
            if (contact != null)
            {
                Token = ConfirmationTokens.Issue("[Повторный:" + Token.ID + "]" + Token.Comment, Token.SystemEvents_Nx);
                contact.Tokens_ID = Token.ID;
                contact.DM = DateTime.Now;
                contact.UM = Account.GetCurrentUserID();
                db.SaveChanges();
                ret = true;
                if (contact.ContactType_Nx == 1 && contact.State != 0)
                {
                    string href = string.Format("http://{0}/Account/Confirmation/{1}?Code={2}",HttpContext.Current.Request.Url.Authority, Token.ID.ToString(), Token.Code.ToString());
                    Messages.Mail.SendMail(contact.Value, "Повторная активация контакта", string.Format("Для активации контакта, пожалуйста, перейдите по <a href='{0}'>ссылке</a> <br>{1}", href,string.IsNullOrEmpty(Reason)?"":"Комментарий: "+Reason));
                }
                Account.CheckSystemCache(true);
            }
            return ret;
        }
        public static string HashPassword(string password)
        {
            var prf = KeyDerivationPrf.HMACSHA256;
            var rng = RandomNumberGenerator.Create();
            const int iterCount = 10000;
            const int saltSize = 128 / 8;
            const int numBytesRequested = 256 / 8;

            // Produce a version 3 (see comment above) text hash.
            var salt = new byte[saltSize];
            rng.GetBytes(salt);
            var subkey = KeyDerivation.Pbkdf2(password, salt, prf, iterCount, numBytesRequested);

            var outputBytes = new byte[13 + salt.Length + subkey.Length];
            outputBytes[0] = 0x01; // format marker
            WriteNetworkByteOrder(outputBytes, 1, (uint)prf);
            WriteNetworkByteOrder(outputBytes, 5, iterCount);
            WriteNetworkByteOrder(outputBytes, 9, saltSize);
            Buffer.BlockCopy(salt, 0, outputBytes, 13, salt.Length);
            Buffer.BlockCopy(subkey, 0, outputBytes, 13 + saltSize, subkey.Length);
            return Convert.ToBase64String(outputBytes);
        }

        public static bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            var decodedHashedPassword = Convert.FromBase64String(hashedPassword);

            // Wrong version
            if (decodedHashedPassword[0] != 0x01)
                return false;

            // Read header information
            var prf = (KeyDerivationPrf)ReadNetworkByteOrder(decodedHashedPassword, 1);
            var iterCount = (int)ReadNetworkByteOrder(decodedHashedPassword, 5);
            var saltLength = (int)ReadNetworkByteOrder(decodedHashedPassword, 9);

            // Read the salt: must be >= 128 bits
            if (saltLength < 128 / 8)
            {
                return false;
            }
            var salt = new byte[saltLength];
            Buffer.BlockCopy(decodedHashedPassword, 13, salt, 0, salt.Length);

            // Read the subkey (the rest of the payload): must be >= 128 bits
            var subkeyLength = decodedHashedPassword.Length - 13 - salt.Length;
            if (subkeyLength < 128 / 8)
            {
                return false;
            }
            var expectedSubkey = new byte[subkeyLength];
            Buffer.BlockCopy(decodedHashedPassword, 13 + salt.Length, expectedSubkey, 0, expectedSubkey.Length);

            // Hash the incoming password and verify it
            var actualSubkey = KeyDerivation.Pbkdf2(providedPassword, salt, prf, iterCount, subkeyLength);
            return actualSubkey.SequenceEqual(expectedSubkey);
        }

        private static void WriteNetworkByteOrder(byte[] buffer, int offset, uint value)
        {
            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)(value >> 0);
        }

        private static uint ReadNetworkByteOrder(byte[] buffer, int offset)
        {
            return ((uint)(buffer[offset + 0]) << 24)
                | ((uint)(buffer[offset + 1]) << 16)
                | ((uint)(buffer[offset + 2]) << 8)
                | ((uint)(buffer[offset + 3]));
        }


        public static string GetAuthority(Guid? UserID, bool IsWithUserName=false)
        {

            string UserName="";
            if (UserID != null)
            {
                var db = CC.CityConstDB;

                var User = Account.GetUser(UserID.Value);
                //var profile = db.Profiles.FirstOrDefault(f => f.Users_ID == UserID && f.State == 1);
                var profile = ProfilesCache.FirstOrDefault(f => f.Users_ID == UserID && f.State == 1);
                UserName = profile == null || string.IsNullOrEmpty(profile.FirstName) ? (User == null ? "без имени" : User.UserName) : string.Format("{0}{1}{2}", profile.FirstName, string.IsNullOrEmpty(profile.SecondName) ? "" : " " + profile.SecondName.ToUpper()[0] + ".", string.IsNullOrEmpty(profile.ThirdName) ? "" : " " + profile.ThirdName.ToUpper()[0] + ".") + (IsWithUserName && User != null ? " (" + User.UserName + ")" : "");
            }
            return UserName;
        }
        public static string GetAuthority(int ProfileNx, bool IsWithUserNameOrShortName = false)
        {

            string UserName;
            var db = CC.CityConstDB;
          
            //var profile = db.Profiles.Single(s=>s.Nx==ProfileNx);
            var profile = ProfilesCache.Single(s => s.Nx == ProfileNx);
            if (profile.ProfileType_Nx == 1)
            {
                Models.Users User = profile.Users_ID != null ? Account.GetUser(profile.Users_ID.Value) : null;
                UserName = string.IsNullOrEmpty(profile.FirstName) ? (User == null ? "без имени" : User.UserName) : string.Format("{0}{1}{2}", profile.FirstName, string.IsNullOrEmpty(profile.SecondName) ? "" : " " + profile.SecondName.ToUpper()[0] + ".", string.IsNullOrEmpty(profile.ThirdName) ? "" : " " + profile.ThirdName.ToUpper()[0] + ".") + (IsWithUserNameOrShortName && User != null ? " (" + User.UserName + ")" : "");
            }
            else
            {
                var Org = profile.Organizations.Single();
                UserName = (IsWithUserNameOrShortName ? (profile.SecondName!=null && profile.SecondName.Count() > 0 ? profile.SecondName : profile.FirstName) : Org.Name) + ", " + Org.BusinessLegalForms.Name;
            }
            return UserName;
        }

        public static void NotifyAdmins()
        {
            //var Clients = CC.WebClients.Where(wh => Account.UsersCache.Any(any => wh.WebSocketContext != null && any.UserName == wh.WebSocketContext.User.Identity.Name && any.State > 0 && any.UsersInRoles.Any(an => an.Roles.RoleName == "Sys.Admin" || an.Roles.RoleName == "Client.Admin"))).ToList();
            var Clients = CC.WebClients.Where(wh => Account.UsersCache.Any(any => any.ID == wh.UserID && any.State > 0 && any.UsersInRoles.Any(an => an.Roles.RoleName == "Sys.Admin" || an.Roles.RoleName == "Client.Admin"))).ToList();
            if (Clients != null && Clients.Count() > 0)
                Clients.ForEach(client => Send(client.Socket,"UPDATEGVONLINEUSERS"));
        }

        public static void Send(WebSocket socket, string Message)
        {
           var buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(Message));
            socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public static  async Task ProcessWebsocketSession(AspNetWebSocketContext context)
        {
            WebSocket socket = context.WebSocket;
            CC.WebClients.Add(new CC.WebSocketClient { Socket=socket, UserID=GetCurrentUserID().Value });
           // Account.NotifyAdmins();
            while (true)
            {
                var buffer = new ArraySegment<byte>(new byte[1024]);
                WebSocketReceiveResult result = await socket.ReceiveAsync(buffer, CancellationToken.None);
                if (socket.State != WebSocketState.Open)
                {
                    CC.WebClients.Remove(CC.WebClients.Single(s => s.Socket == socket));
                    Account.NotifyAdmins();
                    break;
                    //string userMessage = Encoding.UTF8.GetString(buffer.Array, 0, result.Count);
                    //userMessage = "You sent: " + userMessage + " at " + DateTime.Now.ToLongTimeString();
                    //buffer = new ArraySegment(Encoding.UTF8.GetBytes(userMessage));
                    //await socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                }
               
            }
        }
    }

   


}


[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
public class CustomAuthorize : AuthorizeAttribute
{
    private new List<string> Roles;
    private ActionResult unAuthorizedUrl;
    public CustomAuthorize(params string[] MyRoles)
    {
        unAuthorizedUrl = null;
        if (MyRoles.Length > 0)
        {
            Roles = MyRoles.ToList();
        }
    }
    public CustomAuthorize(ActionResult UnAuthorizedUrl, params string[] MyRoles)
    {
        unAuthorizedUrl = UnAuthorizedUrl;
        if (MyRoles.Length > 0)
        {
            Roles = MyRoles.ToList();
        }
    }
    //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    //{
    //    var routeValues = new RouteValueDictionary(new
    //    {
    //        controller = "Home",
    //        action = "Alert",
    //        //int Type=0, string Title=null, string Header=null, string Message=null, bool IsModal=false
    //        Type = 3,
    //        Title = "Досту запрещен",
    //        Header = "Доступ запрещен",
    //        Message = "У Вас недостаточно полномочий для доступа к этому разделу",
    //        IsModal = false
    //    });
    //    filterContext.Result = new RedirectToRouteResult(routeValues);
    //    // filterContext.Result = new RedirectResult("/Account/Login");
    //}

    public override void OnAuthorization(AuthorizationContext filterContext)
    {
        int AuthorizationResult = AuthorizeCore(filterContext.HttpContext);
        if (AuthorizationResult == 1) {
            var Url=new UrlHelper(new RequestContext(filterContext.HttpContext, new RouteData()));
            string UrlPth = Url.Action("Alert", "Home", new { Type = 3, Title = "Ошибка авторизации", Header = "Доступ запрещен", Message = "У Вас недостаточно полномочий для доступа к этому разделу", IsModal = false });
            filterContext.Result =unAuthorizedUrl ?? new RedirectResult(UrlPth);
        }
        else if (AuthorizationResult == 0)
        {
            var Url = new UrlHelper(new RequestContext(filterContext.HttpContext, new RouteData()));
            string UrlPth = Url.Action("Login", "Account", new { ReturnUrl= filterContext.HttpContext.Request.Url.OriginalString });
            filterContext.Result =  new RedirectResult(UrlPth);
        }
    }

    protected new  int AuthorizeCore(HttpContextBase httpContext)
    {

        if (httpContext == null)
        {
            throw new ArgumentNullException("httpContext");
        }
        IPrincipal user = httpContext.User;
        if (!user.Identity.IsAuthenticated)
        {
            return 0;
        }

        if ((Roles != null && Roles.Count > 0) && !Roles.Any(any => user.IsInRole(any)))
        {
            return 1;
        }
        return 2;
    }


  
}
/*
public class WebSocket : WebSocketHandler
{
    public struct WebSocketClient { public WebSocketHandler Socket; public Guid UserID; }
    public WebSocket(List<WebSocketClient> Clients)
    {
        // var ContextUserName =this.WebSocketContext.User.Identity.Name;

        // if (!Clients.Any(any => any.WebSocketContext.User.Identity.Name == ContextUserName))
        //    Clients.Add(this);
        var UserID = Account.GetCurrentUserID();
        if (UserID != null)
        {
           // if (!Clients.Any(any => any.UserID == UserID ))
                Clients.Add(new WebSocketClient { Socket=this, UserID=UserID.Value });
        }
    }

    public override void OnClose()
    {

        Common.WebClients.Remove(Common.WebClients.Single(s=>s.Socket==this));

        Account.NotifyAdmins();

    }
    public override void OnOpen()
    {
    }
}*/