﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using MvcCityConst.Models;
using CC = MvcCityConst.Common;
namespace MvcCityConst.Messages
{
    public class Mail
    {
        public static void SendMail(string Email, string Subject, string Body, bool AddLogo = true)
        {

            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = false;

            //msg.From = new MailAddress("portal@city-const.ru", "City Construction Online System"); 
            msg.From = new MailAddress("city-const@mail.ru", "City Construction Online System");
            msg.Subject = Subject + (AddLogo ? " [City Construction portal]" : "");
            msg.IsBodyHtml = true;
            List<string> Emails = Email.Split(';').Distinct().ToList();


            Emails.ForEach(email =>
            {

                msg.Body = Body;
                msg.To.Clear();
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                bool IsValid = regex.Match(email).Success;
                if (IsValid)
                    msg.To.Add(email);
                var smtp = new SmtpClient { Timeout = 20000, DeliveryMethod = SmtpDeliveryMethod.Network };
                try
                {
                    smtp.Send(msg);
                }
                catch(Exception e)
                {
                    Account.AddSystemLog(15,"Пользователь: "+Account.GetAuthority(Account.GetCurrentUserID(),true)+"\n"+"Время: " +DateTime.Now+"\n"+ e.Source + "\n" + e.Message + "\n" + (e.InnerException!=null? e.InnerException.Source + "\n" + e.InnerException.Message + "\n" + e.StackTrace:""), Account.GetCurrentUserID());
                }
                // Task t = Task.Factory.StartNew(() => { (new SmtpClient { Timeout=20000, DeliveryMethod= SmtpDeliveryMethod.Network }).SendMailAsync(msg); });
                //  bool rez = t.Wait(20000);

            });
        }

        public static Guid SendMessage(int SenderProfile_Nx, int MessageType_Nx, string Subject, string Text, List<int> RecepientProfiles_Nx, int Priority = 1, Guid? OriginalMessage_ID = null, int IsForvarded = 0, bool DublicateToMail=false)
        {
            var db = CC.CityConstDB;
            Guid Message_ID = Guid.NewGuid();
            var CurUser_ID = Account.GetCurrentUserID();
            bool LogOutNotif = false;

            var Message = db.Messages.Add(new Models.Messages { DI = DateTime.Now, TimeStamp = DateTime.Now, RelatedMessage_ID = OriginalMessage_ID, IsForvarded = IsForvarded, ID = Message_ID, MessageTypes_Nx = MessageType_Nx, Priority = Priority, SenderProfiles_Nx = SenderProfile_Nx, State = 1, Subject = Subject, Text = Text, UI = CurUser_ID });
            RecepientProfiles_Nx.ForEach(rec =>
            {
                db.MessageRecepients.Add(new Models.MessageRecepients { DI = DateTime.Now, Messages_ID = Message_ID, RecepientProfile_Nx = rec, State = 1, UI = CurUser_ID });
            });
            db.SaveChanges();
            var Profiles = Account.ProfilesCache.Where(any => RecepientProfiles_Nx.Any(an => an == any.Nx) && any.State > 0);
            var Users = Account.UsersCache.Where(wh => Profiles!= null && wh.State > 0 && Profiles.Count()>0 && Profiles.Any(any => any.Users_ID == wh.ID));
            // var Clients = CC.WebClients.Where(w => Users !=null && Users.Count()>0 && Users.Any(a => a.UserName == w.WebSocketContext.User.Identity.Name)).ToList();
            var Clients = CC.WebClients.Where(w => Users != null && Users.Count() > 0 && Users.Any(a => a.ID == w.UserID)).ToList();
            if (Clients!=null && Clients.Count>0)
                Clients.ForEach(client => Account.Send(client.Socket, "NEWMESSAGES"));
             Clients = Clients.Where(w => !CC.WebClients.Any(any => any.Socket == w.Socket)).ToList();
            if(Clients!=null && Clients.Count > 0 && !DublicateToMail)
            {
                Users = Account.UsersCache.Where(wh => Clients.Any(any => any.UserID== wh.ID) && wh.State > 0).ToList();
                if(Users!=null && Users.Count() > 0)
                {
                    Profiles = Account.ProfilesCache.Where(wh => Users.Any(any => any.ID == wh.Users_ID) && wh.State > 0).ToList();
                    if(Profiles != null && Profiles.Count() > 0)
                    {
                        DublicateToMail = true;
                        RecepientProfiles_Nx = Profiles.Select(sel => sel.Nx).ToList();
                        LogOutNotif = true;
                    }
                }
            }

            if (DublicateToMail)
            {
                string Emails = "";
                RecepientProfiles_Nx.ForEach(prof => {
                    Emails += Account.GetUsersNotifContact(Account.GetUser(prof).ID) + ";";
                });
                Emails = Emails.TrimEnd(';');
                Text = LogOutNotif ? "<b>У Вас на портале City Construction имеется непрочитанное сообщение</b><br/><hr/>" + Text : Text;
                SendMail(Emails, Subject, Text);
            }
            return Message_ID;
        }

        public static void SendMessageBySubscription(int SubscriptionType_Nx, Models.Messages Message)
        {
            var db = CC.CityConstDB;
            var sub = db.Subscriptions.Where(wh => wh.State == 1).ToList();
            sub.ForEach(item => {
                if (item.SubscriptionInterval.Interval > 0)
                {
                    CC.EventAndData EAD = new CC.EventAndData();
                    EAD.SubMethod = SendMessage;
                    EAD.Data = Message;
                    CC.SubscribeOnEvent(EAD, item.SubscriptionInterval);
                }
                else
                {
                    SendMessage(Message);
                }
            });
        }
        //private static void sendMessage<Models.Messages>()
        private static void SendMessage(dynamic msg)
        {
            Models.Messages message = msg;
            SendMessage(message.SenderProfiles_Nx, message.MessageTypes_Nx, message.Subject, message.Text, message.MessageRecepients.Select(sel => sel.RecepientProfile_Nx).ToList(), message.Priority, message.RelatedMessage_ID, message.IsForvarded, DublicateToMail: true);
        }

        public static string HtmlMessageRow(Guid MessageID, bool IsShort = true)
        {
            string ret = "";
            var db = CC.CityConstDB;
            var Message = db.Messages.Single(s => s.ID == MessageID);
            if (Message != null)
            {
                ret = string.Format("<span style='cursor:pointer;color:blue;text-decoration:underline;' onclick=\"$.post('{0}',{{ID:'{1}'}},function(data){{ data='<div class=\\\'ViewMessage\\\'>'+data+'</div>'; $('body').append(data);}});\">{2}{3}</span>", "/Messages/ViewMessage", MessageID.ToString(), IsShort ? "" : "Дата: " + Message.TimeStamp.ToString("dd.MM.yyyy, HH:mm") + " Отправитель: " + Account.GetAuthority(Message.SenderProfiles_Nx) + " ", "Тема: " + Message.Subject);
            }
            return ret;
        }
        public static string HtmlMessageBody(Guid MessageID)
        {
            string Text = "";
            var db = CC.CityConstDB;
            var Message = db.Messages.Single(s => s.ID == MessageID);
            var profile = Message.Profiles;
            var recprofile = Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value);
            string RecCount =( Message.MessageRecepients.Count() - 1).ToString();
            string TheEnding = (RecCount[RecCount.Length - recprofile==null?1:0] == '1' ? "ь" : (Enumerable.Range(2, 4).Contains(int.Parse(RecCount[RecCount.Length - recprofile == null ? 1 : 0].ToString())) ? "я" : "ей"));
            string ToProfile = recprofile == null ? (RecCount + " получател"+ TheEnding) : Account.GetAuthority(recprofile.Value) + (int.Parse(RecCount) > 1 ? string.Format(" и еще {0} получател{1}", RecCount, TheEnding) : "");
            string FromProfile = Account.GetAuthority(profile.Nx);
          
            Text += string.Format("<div style='padding:5px; border:1px solid gray; border-radius: 8px;'>Дата: {0}<br/>От: {1}<br/>Кому: {2}<br/>Тема: {3}<br/><br>{4}</div>", Message.TimeStamp.ToString("dd.MM.yyyy HH:mm"), FromProfile, ToProfile, Message.Subject, Message.Text);

            return Text;
        }
    }
}
    
