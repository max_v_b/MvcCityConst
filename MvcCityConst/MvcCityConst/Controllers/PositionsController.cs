﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;
namespace MvcCityConst.Controllers
{
    public class PositionsController : Controller
    {
        // GET: Positions
        public ActionResult PositionsControl(int Profiles_Nx, bool IsReadOnly)
        {
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView();
        }
        public ActionResult gvPositions(int Profiles_Nx, bool IsReadOnly)
        {
            var db = CC.CityConstDB;
            ViewBag.IsReadOnly = IsReadOnly;
            ViewBag.Profiles_Nx = Profiles_Nx;
            var Model = db.Positions.Where(wh => wh.Organizations.Profiles_Nx == Profiles_Nx).Select(sel => new { sel.Nx, sel.Name, sel.Comments }).ToList();
            return PartialView(Model);
        }
        public ActionResult EditPositions(int Profiles_Nx, int? Nx = null, bool IsReadOnly = true)
        {
            var db = CC.CityConstDB;
            Models.Positions Model = new Models.Positions();
            if (Nx != null)
                Model = db.Positions.Find(Nx.Value);
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Model);
        }
        public ActionResult CheckPositionName(int Profiles_Nx, string PositionName, int? Nx)
        {
            var db = CC.CityConstDB;
            PositionName = CC.NormalizeName(PositionName);
            var rez = db.Positions.Where(any => any.State > 0 && any.Organizations.Profiles_Nx == Profiles_Nx && (Nx == null || any.Nx != Nx.Value)).ToList().Any(any => CC.NormalizeName(any.Name) == PositionName) ? "ERROR" : "OK";
            return Content(rez);
        }

        // $.get("/Positions/SavePosition", { Profiles_Nx: '@ViewBag.Profile_Nx', Nx: $('#Position_Nx').val(), PositionName: $('#PositionName').val(), Comments: $('#PositionComments').val() }

        public ActionResult SavePosition(int Profiles_Nx, int? Nx, string PositionName, string Comments)
        {
            var db = CC.CityConstDB;
            Models.Positions pos;
            if (Nx == null || Nx == 0)
            {
                pos = new Models.Positions { Comments = Comments, Name = PositionName, State = 1, DI = DateTime.Now, UI = Account.GetCurrentUserID(), Organizations = db.Organizations.First(f => f.Profiles_Nx == Profiles_Nx) };
                db.Positions.Add(pos);
            }
            else
            {
                pos = db.Positions.Find(Nx.Value);
                pos.Name = PositionName;
                pos.Comments = Comments;
                pos.DM = DateTime.Now;
                pos.UM = Account.GetCurrentUserID();
            }
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
        public ActionResult DeletePosition(int Nx)
        {
            var db = CC.CityConstDB;
            var item = db.Positions.Find(Nx);
            item.State = 0;
            item.DM = DateTime.Now;
            item.UM = Account.GetCurrentUserID();
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
        public ActionResult cbPositions(string InputValueName, int Profiles_Nx, int? Nx, bool IsReadOnly = false)
        {
            var db = CC.CityConstDB;
            var Model=db.Positions.Where(wh => wh.Organizations.Profiles_Nx == Profiles_Nx && wh.State>0).ToList();
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.InputValueName = InputValueName;
            ViewBag.Nx = Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Model);
        }
    }
}