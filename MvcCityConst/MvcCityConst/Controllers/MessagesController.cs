﻿using MvcCityConst.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class MessagesController : Controller
    {
        //[CustomAuthorize("Sys.Admin","Client.Admin")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult gvIncomingMessages()
        {
            var db = CC.CityConstDB;
            var Cur_ID = Account.GetCurrentUserID().Value;
            var profile =/* db.Profiles*/Account.ProfilesCache.Single(s => s.State == 1 && s.Users_ID == Cur_ID);
            // db.Messages.Where(wh => wh.State != 0 && wh.MessageRecepients.Any(any => any.RecepientProfile_Nx == profile.Nx && any.State != 0)).ToList()
            var Model = db.Messages.Where(wh => /*wh.State == 1 && */(/*wh.MessageRecepients.Count == 0 ||*/ wh.MessageRecepients.Any(any => any.State >0 && (any.RecepientProfile_Nx == profile.Nx /*|| any.Profiles.Employees.Any(a => a.Profile_Nx == profile.Nx)*/)))).ToList()
                .Select(sel => new { sel.ID, sel.Priority, RecepientMessageNx = sel.MessageRecepients.First(f => f.RecepientProfile_Nx == profile.Nx).Nx, SenderProfileNx = sel.Profiles.Nx, sel.RelatedMessage_ID, RelatedMessageSubject = sel.RelatedMessage_ID == null ? "" : sel.Messages2.Subject, sel.IsForvarded, SenderName =Account.GetAuthority(sel.Profiles.Nx), sel.Subject, sel.TimeStamp, sel.MessageTypes_Nx, State = sel.MessageRecepients.First(f => f.RecepientProfile_Nx == profile.Nx).State })
                .OrderByDescending(ob => ob.TimeStamp).ThenBy(tbd => tbd.State).ThenBy(tb => tb.Priority).ThenBy(tb => tb.SenderName);
            return View(Model);
        }
        public ActionResult gvOutgoingMessages()
        {
            var db = CC.CityConstDB;
            var Cur_ID = Account.GetCurrentUserID().Value;
            var profile =/* db.Profiles*/Account.ProfilesCache.Single(s => s.State == 1 && s.Users_ID == Cur_ID);
            var Model = db.Messages.Where(wh => wh.State == 1 && wh.SenderProfiles_Nx == profile.Nx).ToList().Select(sel => new { sel.TimeStamp, sel.RelatedMessage_ID, RelatedMessageSubject = (sel.RelatedMessage_ID == null ? "" : sel.Messages2.Subject), sel.IsForvarded, sel.Priority, sel.ID, Recepients = sel.MessageRecepients.Select(s => Account.GetAuthority(s.RecepientProfile_Nx)).Aggregate((x, y) => x + ", " + y), sel.MessageTypes_Nx, sel.Subject })
                 .OrderByDescending(ob => ob.TimeStamp).ThenBy(tb => tb.Priority).ThenBy(tb => tb.Recepients); 
            return View(Model);
        }
        public ActionResult ViewMessage(Guid ID)
        {
            var db = CC.CityConstDB;
            var Message = db.Messages.Single(s => s.ID == ID);
            var UserID = Account.GetCurrentUserID();
            // var CurrentProfile = db.Profiles.Single(s => s.Users_ID == UserID.Value).Nx;
            var CurrentProfile = Account.GetUserProfile_Nx(UserID.Value);
            var MsgRec = Message.MessageRecepients.FirstOrDefault(s => s.RecepientProfile_Nx == CurrentProfile);
            if (MsgRec!=null && MsgRec.State == 1)
            {
                MsgRec.DM = DateTime.Now;
                MsgRec.UM = Account.GetCurrentUserID();
                MsgRec.State = 2;
                db.SaveChanges();
                var Client = CC.WebClients.SingleOrDefault(s => s.UserID ==UserID.Value);
               // if (Client != null)
                   Account.Send(Client.Socket,"MESSAGES");
                if (Message.MessageTypes_Nx == 3)
                {
                    Mail.SendMessage(db.Profiles.Single(s => s.ID == Account.SystemProfileID).Nx, 2, "Уведомление о просмотре сообщения", string.Format("Ваше <span style='cursor:pointer;color:blue;text-decoration:underline;' onclick=\"$.post('{1}',{{ID:'{2}'}},function(data){{ data='<div class=\\\'ViewMessage\\\'>'+data+'</div>'; $('body').append(data);}});\">сообщение</span> получатель {3} просмотрел {0}.", MsgRec.DM.Value.ToString("dd.MM.yyyy в HH:mm"), Url.Action("ViewMessage", "Messages"), Message.ID, Account.GetAuthority(Account.GetCurrentUserID().Value)), new List<int> { Message.SenderProfiles_Nx });
                }
            }
            ViewBag.ViewID =Math.Abs( DateTime.Now.GetHashCode());
            return View(Message);
        }
        public ActionResult NewMessage(int? ViewID = null)
        {
            var db = CC.CityConstDB;
            var Message = new Models.Messages();
            var CurUserID = Account.GetCurrentUserID();
            //Message.SenderProfiles_Nx = db.Profiles.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            Message.SenderProfiles_Nx = Account.ProfilesCache.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            ViewBag.ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.Type = 1;
            return PartialView("NewMessage", Message);
        }

        public ActionResult ReplyMessage(string Messages, int? ViewID = null)
        {
            string lMessages = "";
            var db = CC.CityConstDB;
            var MsgArr = Messages.TrimEnd(';').Split(';');
            Guid Msg = Guid.Parse(MsgArr[0]);
            ViewBag.Recepients = "";
            if (MsgArr.Length > 1)
                MsgArr.ToList().ForEach(f => {
                    var GuidMsg = Guid.Parse(f);
                    lMessages += Mail.HtmlMessageRow(GuidMsg, false) + "<br/>";
                    var StrProfNx = db.Messages.Single(s => s.ID == GuidMsg).SenderProfiles_Nx.ToString() + ";";
                    if (!((String)ViewBag.Recepients).Contains(StrProfNx))
                        ViewBag.Recepients += StrProfNx;
                });
            else
            {
                ViewBag.Recepients += db.Messages.Single(s => s.ID == Msg).SenderProfiles_Nx.ToString() + ";";
                lMessages = Mail.HtmlMessageBody(Msg);
            }
           
            var Message = new Models.Messages();
            var CurUserID = Account.GetCurrentUserID();
            Message.SenderProfiles_Nx = db.Profiles.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            Message.SenderProfiles_Nx = Account.ProfilesCache.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            string NumOfMsg = MsgArr.Length.ToString();
            Message.Subject="Re: "+ (MsgArr.Length>1? NumOfMsg + " сообщени"+ (NumOfMsg[NumOfMsg.Length - 1]=='1'?"е":(Enumerable.Range(2,4).Contains(int.Parse(NumOfMsg[NumOfMsg.Length-1].ToString()))?"я":"й")):db.Messages.Single(s=>s.ID== Msg).Subject);
            ViewBag.ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.AppliedMessages = lMessages;
            ViewBag.Type = 2;
            ViewBag.MsgList = Messages;
            return PartialView("NewMessage", Message);
        }
        public ActionResult DeleteMessage(string Messages,int TubNum,  int? ViewID = null)
        {
            var db = CC.CityConstDB;
            var MsgArr = Messages.TrimEnd(';').Split(';');
            if (TubNum == 2)
            {
                MsgArr.ToList().ForEach(f =>
                {
                    var GuidMsg = Guid.Parse(f);

                    var msg = db.Messages.Single(s => s.ID == GuidMsg);
                    msg.State = 0;
                    msg.UM = Account.GetCurrentUserID();
                    msg.DM = DateTime.Now;

                });
            }
            else
            {
                var RecProfile_Nx = Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value);
                MsgArr.ToList().ForEach(f =>
                {
                    var GuidMsg = Guid.Parse(f);

                    var msgrec = db.MessageRecepients.First(wh => wh.State != 0 && wh.Messages_ID == GuidMsg && wh.RecepientProfile_Nx == RecProfile_Nx);
                    msgrec.State = 0;
                    msgrec.UM = Account.GetCurrentUserID();
                    msgrec.DM = DateTime.Now;

                });
            }
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
        public ActionResult ForwardMessage(string Messages, int? ViewID = null)
        {
            string lMessages = "";
            var db = CC.CityConstDB;
            var MsgArr = Messages.TrimEnd(';').Split(';');
            Guid Msg = Guid.Parse(MsgArr[0]);
            ViewBag.Recepients = "";
            if (MsgArr.Length > 1)
                MsgArr.ToList().ForEach(f => {
                    var GuidMsg = Guid.Parse(f);
                    lMessages += Mail.HtmlMessageRow(GuidMsg, false) + "<br/>";
                    var StrProfNx = db.Messages.Single(s => s.ID == GuidMsg).SenderProfiles_Nx.ToString() + ";";
                 
                });
            else
            {
                lMessages = Mail.HtmlMessageBody(Msg);
            }

            var Message = new Models.Messages();
            var CurUserID = Account.GetCurrentUserID();
            Message.SenderProfiles_Nx = db.Profiles.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            Message.SenderProfiles_Nx =Account.ProfilesCache.Single(s => s.Users_ID == CurUserID && s.State != 0).Nx;
            string NumOfMsg = MsgArr.Length.ToString();
            Message.Subject = "Fwd: " + (MsgArr.Length > 1 ? NumOfMsg + " сообщени" + (NumOfMsg[NumOfMsg.Length - 1] == '1' ? "е" : (Enumerable.Range(2, 4).Contains(int.Parse(NumOfMsg[NumOfMsg.Length - 1].ToString())) ? "я" : "й")) : db.Messages.Single(s => s.ID == Msg).Subject);
            ViewBag.ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.AppliedMessages = lMessages;
            ViewBag.Type = 3;
            ViewBag.MsgList = Messages;
            return PartialView("NewMessage", Message);
        }
        public ActionResult ViewRecepients(string targetName,string Values, string valuesName, int ViewID)
        {
            // ViewBag.ViewID = ViewID;
            //ViewBag.targetName = targetName;
            //ViewBag.valuesName = valuesName;
            OptionalData od = new OptionalData { targetName = targetName,Values= Values, valuesName = valuesName, ViewID = ViewID };
            return PartialView(od);
        }

        public ActionResult gvRecepient(int ViewID, string Values, string valuesName)
        {
            var db = CC.CityConstDB;
            if (!string.IsNullOrEmpty(Values) || !string.IsNullOrEmpty(valuesName))
            {
                if (!string.IsNullOrEmpty(Values))
                {
                    Values = Values.TrimEnd(';');
                    Session["gvRecepient" + ViewID] = Values.Split(';').Where(wh=>!string.IsNullOrEmpty(wh)).ToList().Select(sel => new { Nx = int.Parse(sel) }).Select(sel => new { sel.Nx, Name = sel.Nx == 0 ? "Все" : (Account.GetAuthority(sel.Nx, true) + (/*db.Profiles.*/ Account.ProfilesCache.Single(s => s.State != 0 && s.Nx == sel.Nx).ProfileType_Nx == 2 ? "(все сотрудники)" : "")) }).ToList();
                }
                else
                    Session["gvRecepient" + ViewID] = null;
            }

            var Model = Session["gvRecepient" + ViewID];
            ViewBag.ViewID = ViewID;
            ViewBag.valuesName = valuesName;
            return View(Model);
        }

        public ActionResult RecepientRow(string Values)
        {
            var db = CC.CityConstDB;
            string ret = "Добавить получателя...";
            if (!string.IsNullOrEmpty(Values))
            {
                Values = Values.TrimEnd(';');
                ret = Values.Split(';').Where(wh => !string.IsNullOrEmpty(wh)).ToList().Select(sel => new { Nx = int.Parse(sel) }).Select(sel => sel.Nx == 0 ? "Все" : (Account.GetAuthority(sel.Nx, true) + (db.Profiles.Single(s => s.State != 0 && s.Nx == sel.Nx).ProfileType_Nx == 2 ? "(все сотрудники)" : ""))).ToList().Aggregate((x, y) => x + "; " + y);
            }
            return Content(ret);
        }

        public ActionResult SendMessage(int SenderProfiles_Nx, string RecepientsProfiles, string Subject, string MessageBody, int NewMessageType,  string IsNotified = "", string HighPriority = "",string MessageList="")
        {
            if (!string.IsNullOrEmpty(RecepientsProfiles) || User.IsInRole("Sys.Admin"))
            {
                var db = CC.CityConstDB;
                MessageBody = "<div>"+ MessageBody+ "</div>";
                int Priority = string.IsNullOrEmpty(HighPriority) ? 1 : 0;
                int MessageType= string.IsNullOrEmpty(IsNotified) ? 1 : 3;
                var MsgArr = MessageList.TrimEnd(';').Split(';').Distinct().ToList();
                List<int> ProfListVal;
                if (string.IsNullOrEmpty(RecepientsProfiles))
                {
                    ProfListVal = db.Organizations.Where(wh => wh.State != 0 && wh.Profiles_Nx!=null).Select(sel => sel.Profiles_Nx.Value).ToList();
                }
                else
                     ProfListVal = RecepientsProfiles.TrimEnd(';').Split(';').ToList().Select(sel => int.Parse(sel)).Distinct().ToList();

                ProfListVal.ForEach(Profile_Nx =>
                {
                    var Profile =/* db.Profiles*/ Account.ProfilesCache.FirstOrDefault(s => s.Nx == Profile_Nx && s.State > 0);
                    if (Profile != null)
                    {
                        if (Profile.ProfileType_Nx == 2)
                        {
                            var ProfEmplList = (Profile.Organizations.Count > 0 ? Profile.Organizations.First().Employees.Where(wh => wh.State == 1 && wh.Profiles.State == 1).Select(sel => sel.Profiles.Nx).ToList() : null);

                            if (ProfEmplList != null && ProfEmplList.Count()>0)
                            {
                                ProfListVal.InsertRange(ProfListVal.IndexOf(Profile_Nx), ProfEmplList);
                            }
                            ProfListVal.Remove(Profile_Nx);
                        }
                    }
                });
                ProfListVal = ProfListVal.Distinct().ToList();
                if (MsgArr.Count() > 1)
                    MsgArr.ToList().ForEach(item => MessageBody += Mail.HtmlMessageRow(Guid.Parse(item)) + "; ");
                else
                    MessageBody += MsgArr[0].Length == 0 ? "" : Mail.HtmlMessageBody(Guid.Parse(MsgArr[0]));
                Mail.SendMessage(SenderProfiles_Nx, MessageType, Subject, MessageBody, ProfListVal, Priority, OriginalMessage_ID:(MsgArr.Count()== 1 && MsgArr[0].Length>0? (Guid?)Guid.Parse(MsgArr[0]):null), IsForvarded: NewMessageType==3?1:0);

                return new HttpStatusCodeResult(200);
            }
            //Alert(int Type=0, string Title=null, string Header=null, string Message=null)
            return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Ошибка отправки сообщения", Header = "Ошибка отправки сообщения", Message = "Сообщение не было отправлено. Причина: Не указаны получатели. У Вас нет прав делать рассылку на всех пользователей системы." });
        }

        public ActionResult GetUnreadedMessages()
        {
            string ret = "";
            var db = CC.CityConstDB;
            var Cur_ID = Account.GetCurrentUserID().Value;
            var profile = /*db.Profiles*/Account.ProfilesCache.Single(s => s.State == 1 && s.Users_ID == Cur_ID);
            int UnreadedMessages = db.Messages.Where(wh => wh.State != 0 && (wh.MessageRecepients.Count==0 || wh.MessageRecepients.Any(any => any.State == 1 && (any.RecepientProfile_Nx == profile.Nx  || any.Profiles.Employees.Any(a=>a.Profile_Nx==profile.Nx))))).Count();
            if (UnreadedMessages > 0)
                ret =string.Format("<span title='У Вас есть непрочтенные сообщения' class='label label-danger label-as-badge' >{0}</span>", UnreadedMessages);
            return Content(ret);
        }

    }
}