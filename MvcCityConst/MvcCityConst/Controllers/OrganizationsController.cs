﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class OrganizationsController : Controller
    {
        public ActionResult Index()
        {
            if (User.IsInRole("Sys.Admin"))
            {
                ViewBag.IsReadOnly = false;
                int ViewID = Math.Abs(DateTime.Now.GetHashCode());
                ViewBag.ViewID = ViewID;
             
                return View();
            }
            else if(User.IsInRole("Client.Admin"))
            {
                ViewBag.IsReadOnly = false;
                int ViewID = Math.Abs(DateTime.Now.GetHashCode());
                ViewBag.ViewID = ViewID;
                var User_ID = Account.GetCurrentUserID();
                ViewBag.Nx = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User_ID)).Employees.Single().Organizations.Nx;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult OrganizationControl(int? ViewID = null, bool IsReadOnly = false, int? Nx=null)
        {
            ViewBag.IsReadOnly = false;
             ViewID = ViewID?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.ViewID = ViewID;
            ViewBag.Nx = Nx;
            return PartialView();
        }
        [Authorize(Roles = "Sys.Admin,Client.Admin")]
        public ActionResult gvOrganizations(bool Update=false, bool IsReadOnly=false, int? ViewID = null)
        {
            var db = CC.CityConstDB;
           // ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.ViewID = ViewID;
            if (Session["gvOrganizations"+ ViewID] == null || Update)
            {
                var Model = db.Organizations.Where(wh => wh.State > 0).Select(sel => new { sel.Nx, sel.Profiles_Nx, sel.EmployeesMaxNumber, sel.Name, BusinessLegalFormsName = sel.BusinessLegalForms.Name, sel.State, Chief_Profiles_Nx = (sel.Employees.Any(s => s.State > 0 && s.Chief == 1) ? (int?)sel.Employees.FirstOrDefault(s => s.State > 0 && s.Chief == 1).Profile_Nx : null), EmplNumber = sel.Employees.Where(wh => wh.State > 0).Count(), sel.Comments, sel.OrgType.TypeName, sel.OrgType_Nx, Adress = sel.Profiles.Adress }).ToList().
                    Select(sel => new { sel.Adress, sel.Chief_Profiles_Nx, Chief = sel.Chief_Profiles_Nx != null ? Account.GetAuthority(sel.Chief_Profiles_Nx.Value) : "", OrgName = sel.Profiles_Nx == null ? "(" + sel.Name + "), " + sel.BusinessLegalFormsName : Account.GetAuthority(sel.Profiles_Nx.Value), sel.Nx, sel.Profiles_Nx, sel.State, sel.Comments, sel.EmployeesMaxNumber, sel.OrgType_Nx, sel.TypeName, sel.EmplNumber }).OrderBy(ob => ob.OrgType_Nx).ThenBy(tb => tb.OrgName);
                Session["gvOrganizations" + ViewID] = Model;
            }
                ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Session["gvOrganizations" + ViewID]);
        }
        public ActionResult GetOrgState(int Nx)
        {
            var db = CC.CityConstDB;
            var Org = db.Organizations.Find(Nx);
            return Content(Org.State.ToString());
        }
        [Authorize(Roles = "Sys.Admin,Client.Admin")]
        public ActionResult OrganizationEdit(int? Nx, int? ParrentOrg_Nx,  bool IsReadOnly=false, int? ViewID=null)
        {
            var db = CC.CityConstDB;
            ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.ViewID = ViewID;
            Models.Organizations Org;
            if (Nx != null)
            {
                Org = db.Organizations.Single(s => s.Nx == Nx);
                if (Org.Profiles == null)
                {
                    Org.Profiles = db.Profiles.Add(new Models.Profiles { DI = DateTime.Now, FirstName = Org.Name, ID = Guid.NewGuid(), ProfileType_Nx = 2, State = 1, UI = Account.GetCurrentUserID() });
                    db.SaveChanges();
                    Org = db.Organizations.Single(s => s.Nx == Nx);
                    Account.CheckSystemCache(true);
                }
            }
            else
            {
                Org = new Models.Organizations { Nx=0, ParentOrg_Nx= ParrentOrg_Nx };
            }
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Org);
        }
        public struct Org { public int Nx { get; set; } public string Name { get; set; } }
        [AllowAnonymous]
        public ActionResult cbOrganizations(bool Update=false, bool IsWithAll=false, string cbOrganizations_LostFocus="", int? ViewID=null, bool IsUseProfileNx= false, string InputValueName= "")
        {
            //ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            if (Update)
                Session["cbOrganizations"+ ViewID.ToString()] = null;
            var db = CC.CityConstDB;
            var Model =(List<Org>)(Session["cbOrganizations" + ViewID.ToString()] ?? db.Organizations.Where(wh => (wh.State == 1 || wh.State==3) && (!IsUseProfileNx || wh.Profiles_Nx!=null)).OrderBy(ob=>ob.Name).ToList().Select(sel=>new Org{ Nx= !IsUseProfileNx?sel.Nx:sel.Profiles_Nx.Value, Name=sel.Name+", "+sel.BusinessLegalForms.Name }).ToList());
            if (User.IsInRole("Sys.Admin") && IsWithAll && Update)
                Model.Insert(0, new Org { Nx=0, Name="Все" });
            Session["cbOrganizations" + ViewID] = Model;
            ViewData["OrgLostFocusEvent"+ ViewID] = cbOrganizations_LostFocus;
            Session["cbOrganizations" + ViewID] = Model;
            ViewBag.ViewID = ViewID;
            ViewBag.Update = Update;
            ViewBag.IsWithAll = IsWithAll;
            ViewBag.cbOrganizations_LostFocus = cbOrganizations_LostFocus;
            ViewBag.IsUseProfileNx = IsUseProfileNx;
            ViewBag.InputValueName = InputValueName;
            return View(Model);
        }
        [AllowAnonymous]
        public ActionResult cbBusinessLegalForms(string InputValueName="", bool Update = false, int? BusinessLegalFormsNx=null, bool IsReadOnly=false, int? ViewID = null)
        {
           // ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            if (Update)
                Session["cbBusinessLegalForms"+ ViewID] = null;
            var db = CC.CityConstDB;
            var Model = Session["cbBusinessLegalForms" + ViewID] ?? db.BusinessLegalForms.Select(sel => new { sel.Nx, sel.Name }).ToList();
            Session["cbBusinessLegalForms" + ViewID] = Model;
            ViewBag.InputValueName = InputValueName;
            ViewBag.BusinessLegalFormsNx = BusinessLegalFormsNx;
            ViewBag.IsReadOnly = IsReadOnly;
            ViewBag.ViewID = ViewID;
            return View(Model);
        }
        [AllowAnonymous]
        public ActionResult CheckOrgname(string OrgName)
        {
          var ret= Account.CheckOrgName(OrgName) ? "OK" : "ERROR";
           return Content(ret);
        }

        public ActionResult OrgProfile(Models.Organizations Org, bool IsReadOnly=true, string AfterSaveAction="", int? ViewID = null)
        {
            ViewID = ViewID ?? Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.IsReadOnly = IsReadOnly;
            ViewBag.CallBackFunk = AfterSaveAction;
            ViewBag.ViewID = ViewID;
            return PartialView(Org);
        }

        public ActionResult SaveOrgProfile(int? Org_Profiles_Nx, int cbBusinessLegalFormsValue, string Name, string SecondName, string ThirdName,string INN, string Adress, string CodeOgrn,string RegDate, int EmployeesMaxNumber, string Comments, string Email)
        {
            string ret = "OK";
            DateTime? RDate = (DateTime?)(string.IsNullOrEmpty(RegDate) ? null :(DateTime?) DateTime.Parse(RegDate));
            var db = CC.CityConstDB;
            Models.Profiles profile;
            Models.Organizations org;
            if(Org_Profiles_Nx==null)
            {

                var Profile_Nx = Account.RegisterNewOrganization(Name, cbBusinessLegalFormsValue, Email);

                profile = Account.ProfilesCache.Single(s => s.Nx == Profile_Nx);
                org = profile.Organizations.Single();
                profile.FirstName = Name;
                profile.SecondName = SecondName;
                profile.ThirdName = ThirdName;
                profile.INN = INN;
                profile.Adress = Adress;

                org.CodeOGRN = CodeOgrn;
                org.RegDate = RDate;
                org.Comments = Comments;
            }
            else
            {
                profile = db.Profiles.Single(s => s.Nx == Org_Profiles_Nx.Value);
                org = profile.Organizations.Single();
                profile.FirstName = Name;
                profile.SecondName = SecondName;
                profile.ThirdName = ThirdName;
                profile.INN = INN;
                profile.Adress = Adress;
                profile.Comments = Comments;
                profile.UM = Account.GetCurrentUserID();
                profile.DM = DateTime.Now;

                org.Name = Name;
                org.BusinessLegalForms_Nx = cbBusinessLegalFormsValue;
                org.CodeOGRN = CodeOgrn;
                org.RegDate = RDate;
                org.Comments = Comments;
                org.UM = profile.UM;
                org.DM = DateTime.Now;
            }
            db.SaveChanges();
            Account.CheckSystemCache(true);
            return Content(ret);
        }
        //DeleteOrganization', { Nx: e 

        public ActionResult DeleteOrganization(int Nx, Guid? curusr=null, DateTime? dttm_n=null, bool ForceCheckSystemCache = true)
        {
           
            curusr = curusr ?? Account.GetCurrentUserID();
            dttm_n = dttm_n ?? DateTime.Now;
            var db = CC.CityConstDB;
            var Org = db.Organizations.Find(Nx);
            var EC = new EmployeesController();
            Org.Employees.ToList().ForEach(item => EC.DeleteEmployee(item.Nx, curusr, dttm_n, false));
            Org.Organizations1.ToList().ForEach(org => this.DeleteOrganization(org.Nx, curusr, dttm_n, false));
            Org.Profiles.Contacts.ToList().ForEach(cnt => { cnt.State = 0; cnt.UM = curusr; cnt.DM = dttm_n; });
            Org.Profiles.State = 0;
            Org.Profiles.UM = curusr;
            Org.Profiles.DM = dttm_n;
            Org.State = 0;
            Org.UM = curusr;
            Org.DM = dttm_n;
            db.SaveChanges();
            Account.AddSystemLog(17, string.Format("Удаление организации({0}), всех ее контактов({1}), всех сотрудников({2}) и всех филиалов({3})",Account.GetAuthority(Org.Profiles_Nx.Value), Org.Profiles.Contacts.Count.ToString(), Org.Employees.Count.ToString(), Org.Organizations1.Count.ToString()));
            Account.CheckSystemCache(ForceCheckSystemCache);
            return new HttpStatusCodeResult(200);
        }
        public ActionResult RestrictonActions(string Reason, int CmdType, int Nx)
        {
            var db = CC.CityConstDB;
            var sb = CC.SystemModel;
            Guid[] Users_ID;
           Guid[] ClientAdmin;
            //var User = sb.Users.Find(Account.GetUser(db.Employees.Find(Nx).Profile_Nx).Nx);
            var Org = db.Organizations.Find(Nx);
            if (Org.OrgType_Nx != 1)
            {
                switch (CmdType)
                {
                    case 1:
                       
                         Users_ID = Org.Employees.Where(wh => wh.State > 0).Select(sel => sel.Profiles.Users_ID.Value).ToArray();
                        ClientAdmin = sb.Users.Where(fod => Users_ID.Any(any => any == fod.ID) && fod.UsersInRoles.Any(any => any.Roles.RoleName == "Client.Admin" || any.Roles.RoleName == "Sys.Admin")).Select(sel => sel.ID).ToArray();
                        if (ClientAdmin.Count()>0)
                        {
                            Org.State = 1;
                            Org.DM = DateTime.Now;
                            Org.UM = Account.GetCurrentUserID();
                            db.SaveChanges();
                            var Profiles_Nx = Org.Employees.Where(fod1 => ClientAdmin.Any(any=>fod1.Profiles.Users_ID ==any)).Select(sel=>sel.Profile_Nx).ToList();
                            Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Снятие ограничений на доступ", string.Format("Пользователь {0} снял для Вас ограничения на пользование порталом.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), Profiles_Nx, DublicateToMail: true);
                            Account.AddSystemLog(2, "Организация: " + Account.GetAuthority(Org.Profiles_Nx.Value) + " Комментарий" + Reason);
                            Account.CheckSystemCache(true);
                        }
                        break;
                    case 3:
                        Org.State = 2;
                        Org.DM = DateTime.Now;
                        Org.UM = Account.GetCurrentUserID();
                        db.SaveChanges();
                         Users_ID = Org.Employees.Where(wh => wh.State > 0).Select(sel => sel.Profiles.Users_ID.Value).ToArray();
                        ClientAdmin = sb.Users.Where(fod => Users_ID.Any(any => any == fod.ID) && fod.UsersInRoles.Any(any => any.Roles.RoleName == "Client.Admin" || any.Roles.RoleName == "Sys.Admin")).Select(sel => sel.ID).ToArray();
                        if (ClientAdmin != null)
                        {
                            var Profiles_Nx = Org.Employees.Where(fod1 => ClientAdmin.Any(any => fod1.Profiles.Users_ID == any)).Select(sel => sel.Profile_Nx).ToList();
                            Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Полная блокировка организации", string.Format("Пользователь {0} установил блокировку для всей оргаеизации на пользование порталом.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), Profiles_Nx, DublicateToMail: true);
                            Account.AddSystemLog(3, "Организация: " + Account.GetAuthority(Org.Profiles_Nx.Value) + " Комментарий" + Reason);
                            Account.CheckSystemCache(true);
                        }
                        break;
                    case 2:

                        Users_ID = Org.Employees.Where(wh => wh.State > 0).Select(sel => sel.Profiles.Users_ID.Value).ToArray();
                        ClientAdmin = sb.Users.Where(fod => Users_ID.Any(any => any == fod.ID) && fod.UsersInRoles.Any(any => any.Roles.RoleName == "Client.Admin" || any.Roles.RoleName == "Sys.Admin")).Select(sel => sel.ID).ToArray();
                        if (ClientAdmin != null)
                        {
                            Org.State = 3;
                            Org.DM = DateTime.Now;
                            Org.UM = Account.GetCurrentUserID();
                            db.SaveChanges();
                            var Profiles_Nx = Org.Employees.Where(fod1 => ClientAdmin.Any(any => fod1.Profiles.Users_ID == any)).Select(sel => sel.Profile_Nx).ToList();
                            Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Снятие бокировки с одновременным введением ограничения на доступ", string.Format("Пользователь {0} снял полную блокировку организации, однако все еще остаются ограничения для полного доступа к порталу.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), Profiles_Nx, DublicateToMail: true);
                            Account.AddSystemLog(3, "Снятие бокировки с одновременным введением ограничения на доступ. Организация: " + Account.GetAuthority(Org.Profiles_Nx.Value) + " Комментарий" + Reason);
                            Account.CheckSystemCache(true);
                        }
                        break;
                    case 4:

                        Users_ID = Org.Employees.Where(wh => wh.State > 0).Select(sel => sel.Profiles.Users_ID.Value).ToArray();
                        ClientAdmin = sb.Users.Where(fod => Users_ID.Any(any => any == fod.ID) && fod.UsersInRoles.Any(any => any.Roles.RoleName == "Client.Admin" || any.Roles.RoleName == "Sys.Admin")).Select(sel => sel.ID).ToArray();
                        if (ClientAdmin != null)
                        {
                            Org.State = 3;
                            Org.DM = DateTime.Now;
                            Org.UM = Account.GetCurrentUserID();
                            db.SaveChanges();
                            var Profiles_Nx = Org.Employees.Where(fod1 => ClientAdmin.Any(any => fod1.Profiles.Users_ID == any)).Select(sel => sel.Profile_Nx).ToList();
                            Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Введение ограничения на доступ", string.Format("Пользователь {0} ограничил всей организации доступ к порталу.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), Profiles_Nx, DublicateToMail: true);
                            Account.AddSystemLog(3, "Введение ограничения на доступ. Организация: " + Account.GetAuthority(Org.Profiles_Nx.Value) + " Комментарий" + Reason);
                            Account.CheckSystemCache(true);
                        }
                        break;
                }
            }
            return new HttpStatusCodeResult(200);
        }

    }
}