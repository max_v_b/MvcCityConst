﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;
namespace MvcCityConst.Controllers
{
    public class DepartmentsController : Controller
    {
        // GET: Departments
        public ActionResult DepartmentsControl(int Profiles_Nx, bool IsReadOnly)
        {
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView();
        }
        public ActionResult tlDepartments(int? Profiles_Nx, bool IsReadOnly = true)
        {
            if (Profiles_Nx != null)
            {
                var db = CC.CityConstDB;
                var Model = db.Departments.Where(wh => wh.Organizations.Profiles_Nx == Profiles_Nx.Value && wh.State != 0).Select(sel => new { sel.Nx, sel.Name, sel.ParentDepartment_Nx, ParentDepartmenName = sel.Departments2.Name, sel.Comments }).OrderBy(ob => ob.ParentDepartmenName).ThenBy(tb => tb.Name).ToList();
                Session["tlDepartments"] = Model;
                Session["tlDepartments_IsReadOnly"] = IsReadOnly;
            }
            ViewBag.IsReadOnly = (bool)Session["tlDepartments_IsReadOnly"];
            return PartialView(Session["tlDepartments"]);
        }
        public ActionResult EditDepartment(int Profiles_Nx, int? Nx = null, bool IsReadOnly = true)
        {
            var db = CC.CityConstDB;
            Models.Departments Model = new Models.Departments();
            if (Nx != null)
                Model = db.Departments.Find(Nx.Value);
            ViewBag.Profile_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Model);
        }
        //CheckDepartmentName", { Profile_Nx: '@Model.Organizations.Profiles_Nx', DepartmentName: $('#DepartmentName').val(), Department_Nx: '@Model.Nx', ParentDepartment_Nx: cbDepartments.GetValue()

        public ActionResult CheckDepartmentName(int Profiles_Nx, string DepartmentName, int? Department_Nx, int? ParentDepartment_Nx)
        {
            var db = CC.CityConstDB;
            DepartmentName = CC.NormalizeName(DepartmentName);
            var rez = db.Departments.Where(any => any.State > 0 && any.Organizations.Profiles_Nx == Profiles_Nx && (ParentDepartment_Nx == null || any.ParentDepartment_Nx == ParentDepartment_Nx.Value) && (Department_Nx == null || any.Nx != Department_Nx.Value)).ToList().Any(any => CC.NormalizeName(any.Name) == DepartmentName) ? "ERROR" : "OK";
            return Content(rez);
        }

        public ActionResult cbDepartments(string InputValueName, int Profiles_Nx, int? Nx, bool IsReadOnly = false)
        {
            ViewBag.InputValueName = InputValueName;
            ViewBag.Departments_Nx = Nx;
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            var db = CC.CityConstDB;
            var TempModel = db.Departments.Where(wh =>/*(Nx==null || wh.Nx!=Nx.Value) &&*/ wh.Organizations.Profiles_Nx == Profiles_Nx && wh.State > 0)/*.Select(sel=>new {sel.Nx, sel.Name, sel.Organizations_Nx, sel.ParentDepartment_Nx, sel.Departments2, CompaundName="" })*/.ToList();
            TempModel.ForEach(item =>
            {
                item.CompaundName = item.Name;
                var tmpitem = item;
                while (tmpitem.ParentDepartment_Nx != null)
                {
                    item.CompaundName= tmpitem.Departments2.Name + " => " + item.CompaundName;
                    tmpitem = tmpitem.Departments2;

                }
            });
            var Model = TempModel.OrderBy(ob => ob.Name).Select(sel=>new {sel.Nx,Name= sel.CompaundName }).ToList();
            ViewBag.Nx = Nx;
            return PartialView(Model);
        }

        public ActionResult CheckDepartments(int Department_Nx, int ParentDepartment_Nx)
        {
            var db = CC.CityConstDB;
            bool stop = false;
            bool success = true;
            var item = db.Departments.Find(Department_Nx);
            item.ParentDepartment_Nx = ParentDepartment_Nx;
            while (!stop)
            {
                if (item.ParentDepartment_Nx != null)
                {
                    if(item.ParentDepartment_Nx!= Department_Nx)
                        item = db.Departments.Find(item.ParentDepartment_Nx);
                    else
                    {
                        success = false;
                        stop = true;
                    }
                }
                else
                    stop = true;
            }
            return Content(success?"OK":"ERROR");
        }
        //SaveDepartment", { Profiles_Nx: '@ViewBag.Profile_Nx', Department_Nx: $('#Department_Nx').val(), DepartmentName: $('#DepartmentName').val(), ParentDepartment: cbDepartments.GetValue(),  Comments:
        public ActionResult SaveDepartment(int Profiles_Nx, int? Department_Nx, string DepartmentName, int? ParentDepartment_Nx, string Comments)
        {
            var db = CC.CityConstDB;
            Models.Departments dpt;
            if (Department_Nx == null || Department_Nx==0)
            {
                dpt = new Models.Departments { Comments = Comments, ParentDepartment_Nx = ParentDepartment_Nx, Name = DepartmentName, State = 1, DI = DateTime.Now, UI = Account.GetCurrentUserID(), Organizations=db.Organizations.First(f=>f.Profiles_Nx==Profiles_Nx) };
                db.Departments.Add(dpt);
            }
            else
            {
                dpt = db.Departments.Find(Department_Nx.Value);
                dpt.Name = DepartmentName;
                dpt.ParentDepartment_Nx = ParentDepartment_Nx;
                dpt.Comments = Comments;
                dpt.DM = DateTime.Now;
                dpt.UM = Account.GetCurrentUserID();
            }
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }

        public ActionResult DeleteDepartment(int Nx)
        {
            var db = CC.CityConstDB;
            var item = db.Departments.Find(Nx);
            item.State = 0;
            item.DM = DateTime.Now;
            item.UM = Account.GetCurrentUserID();
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
    }

}