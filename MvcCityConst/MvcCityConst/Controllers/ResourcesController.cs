﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class ResourcesController : Controller
    {
        // GET: Resources
        public ActionResult Index(int Profiles_Nx, int Nx)
        {
            //var ds = CC.CityConstModel;
            //ViewBag.Contragent = ds.СпКонтрагенты.First(f => f.IDКонтрагенты == ID);
            //ViewBag.JobID = JobID;
            //var job= ds.Работы.First(f => f.IDРаботы == JobID);
            //ViewBag.JobHeader = (job.СпВидыРабот != null ? job.СпВидыРабот.ВидРаботы:"") + " ("+(job.СпОбуМестоПоложение!=null?job.СпОбуМестоПоложение.ОбуМестоПоложение:"") + ")";

            var db = CC.CityConstDB;
            ViewBag.Org = Account.ProfilesCache.Find(f => f.Nx == Profiles_Nx).Organizations.Single();
            ViewBag.JobsAppointed_Nx = Nx;
            var JA = db.JobsAppointed.Find(Nx);
            ViewBag.JobHeader = (JA.Jobs != null ? JA.Jobs.Name : "") + " (" + (JA.ManagedObjectLocation != null ? JA.ManagedObjectLocation.Name : "") + ")";
            return View();
        }

        public ActionResult gvResources(int? Nx, int? Employees_Nx)
        {
            if (Nx != null)
            {
                //var ds = CC.CityConstModel;
                var db = CC.CityConstDB;
                Session["gvResources"]=db.JobsExecutedDeclared.Where(wh=>wh.State>0 && wh.JobsAppointedWithResources.JobsAppointed_Nx==Nx && (Employees_Nx==null || db.JobsToEmployees.Any(any=>any.State>0 && any.Employees_Nx==Employees_Nx && any.JobsAppointed_Nx==Nx.Value))).Select(sel=>new
                {
                    sel.Nx,
                    Resources_Nx=sel.JobsAppointedWithResources.Resources.Nx,
                    ResourceName=sel.JobsAppointedWithResources.Resources.Name,
                    sel.VolumeDeclared,
                    sel.ExecutionDate,
                    sel.CommentsExecutor,
                    sel.CommentsSupervisor,
                    FactVolume = sel.JobsExecutedConfirmed != null ? sel.JobsExecutedConfirmed.Volume : null,
                    FactDate=sel.JobsExecutedConfirmed != null ? sel.JobsExecutedConfirmed.ExecutionDate:null
                }).OrderByDescending(ob => ob.ExecutionDate).ThenBy(tb => tb.ResourceName).ToList(); ;
                //Session["gvResources"] = ds.РаботаФактПодрядчик.Where(wh => wh.РаботаРесурсОбъем.IDРаботы == JobID.Value && (EmployeeID == null || ds.СпСотрСпРаботы.Any(any => any.IDСпСотрудники == EmployeeID.Value && any.IDРаботы == JobID.Value))).Select(sel => new
                //{
                //    sel.IDРаботаФактПодрядчик,
                //    IDСпРесурсы = sel.РаботаРесурсОбъем.СпРесурсы.IDСпРесурсы,
                //    Ресурс =sel.РаботаРесурсОбъем.СпРесурсы.Ресурс,
                //    sel.ОбъемПодр,
                //    sel.ДатаВыполнения,
                //    sel.КомментарийПодр,
                //    sel.КомментарийКонтроль,
                //    FactVolume = sel.РаботаФакт != null ? sel.РаботаФакт.Объем : null,
                //    FactDate = sel.РаботаФакт != null ? sel.РаботаФакт.ДатаВыполнения : null
                //}).OrderByDescending(ob=>ob.ДатаВыполнения).ThenBy(tb=>tb.Ресурс).ToList();
            }

            return View(Session["gvResources"]);
        }

       

        public ActionResult cbResourcePartial(int JobsAppointed_Nx)
        {
            //var ds = CC.CityConstModel;
            //var Model=ds.СпРесурсы.Where(wh=>wh.РаботаРесурсОбъем.Any(any=>any.IDРаботы==JobID)).Select(sel => new { sel.IDСпРесурсы, sel.Ресурс }).ToList();

            var db = CC.CityConstDB;
            var Model=db.Resources.Where(wh=>wh.State>0 && wh.JobsAppointedWithResources.Any(any=>any.State>0 && any.JobsAppointed_Nx== JobsAppointed_Nx)).Select(sel => new { sel.Nx, sel.Name }).ToList();
            return PartialView("cbResourcePartial",Model);
        }
        public ActionResult AddResource(int JobsAppointed_Nx, int Recourses_Nx, DateTime ResourceDateEdit, float ResourceNumber, string ResourceComment, int? JobsExecutedDeclared_Nx)
        {
           // var ds = CC.CityConstModel;
            var db = CC.CityConstDB;
            var JAR_Nx = db.JobsAppointedWithResources.First(f=>f.Resources_Nx==Recourses_Nx && f.JobsAppointed_Nx==JobsAppointed_Nx).Nx;
            if (JobsExecutedDeclared_Nx == null)
            {
                db.JobsExecutedDeclared.Add(new Models.JobsExecutedDeclared { JobsAppointedWithResources_Nx = JAR_Nx, ExecutionDate = ResourceDateEdit, VolumeDeclared = (Decimal)ResourceNumber, CommentsExecutor = ResourceComment,State=1, DI=DateTime.Now, UI=Account.GetCurrentUserID()});
                db.SaveChanges();
            }
            else
            {
                var JED = db.JobsExecutedDeclared.Find(JobsExecutedDeclared_Nx.Value);
                JED.ExecutionDate = ResourceDateEdit;
                JED.VolumeDeclared = (Decimal)ResourceNumber;
                JED.CommentsExecutor = ResourceComment;
                JED.JobsAppointedWithResources_Nx = JAR_Nx;
                JED.DM = DateTime.Now;
                JED.UM = Account.GetCurrentUserID();
                db.SaveChanges();
            }
            //var JR_ID = ds.РаботаРесурсОбъем.First(f => f.IDРаботы == JobID && f.IDСпРесурсы == RecTypeID).IDРаботаРесурсОбъем;
            //if (ResourceID == null)
            //{
            //    ds.РаботаФактПодрядчик.Add(new Models.РаботаФактПодрядчик {  IDРаботаРесурсОбъем=JR_ID, ДатаВыполнения=ResourceDateEdit, ОбъемПодр=(decimal)ResourceNumber, КомментарийПодр=ResourceComment  });
            //    ds.SaveChanges();
            //}
            //else
            //{
            //    var Rs=ds.РаботаФактПодрядчик.First(f => f.IDРаботаФактПодрядчик == ResourceID);
            //    Rs.ДатаВыполнения = ResourceDateEdit;
            //    Rs.ОбъемПодр = (decimal)ResourceNumber;
            //    Rs.КомментарийПодр = ResourceComment;
            //    Rs.IDРаботаРесурсОбъем = JR_ID;
            //    ds.SaveChanges();
            //}
            return new HttpStatusCodeResult(200);
        }
        public ActionResult DeleteResource(int JobsExecutedDeclared_Nx)
        {
            //var ds = CC.CityConstModel;
            //ds.РаботаФактПодрядчик.Remove(ds.РаботаФактПодрядчик.First(f => f.IDРаботаФактПодрядчик == ResourceID));
            //ds.SaveChanges();
            var db = CC.CityConstDB;
            var JED = db.JobsExecutedDeclared.Find(JobsExecutedDeclared_Nx);
            JED.State = 0;
            JED.DM = DateTime.Now;
            JED.UM = Account.GetCurrentUserID();
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
    }
}