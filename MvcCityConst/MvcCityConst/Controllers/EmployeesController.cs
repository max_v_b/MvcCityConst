﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Employees(int Profiles_Nx, bool IsReadOnly=false)
        {
            var db = CC.CityConstDB;
            ViewBag.Contragent = db.Organizations.FirstOrDefault(fod => fod.Profiles_Nx== Profiles_Nx);
            ViewBag.IsReadOnly = IsReadOnly;
            ViewBag.Profiles_Nx = Profiles_Nx;
            return PartialView();
        }
        public ActionResult Location()
        {
            return View();
        }
      
        public ActionResult cbEmployees(bool Update = false, int? Organizations_Nx=null, bool IsUseProfile=false, bool IsWithAll = false, string cbEmployees_LostFocus = "", int? ViewID = null)
        {
            var db = CC.CityConstDB;
            if (Update)
                Session["cbEmployees" + (ViewID == null ? "" : ViewID.Value.ToString())] = null;
            var Model = (List<OrganizationsController.Org>)(Session["cbEmployees" + (ViewID == null ? "" : ViewID.Value.ToString())] ?? db.Profiles.Where(wh => wh.State != 0 && wh.ProfileType_Nx == 1 && wh.Employees.Any(any => (Organizations_Nx == null || (!IsUseProfile && any.Organizations.Nx == Organizations_Nx.Value || IsUseProfile && any.Organizations.Profiles_Nx == Organizations_Nx.Value)) ))
                .ToList().Select(sel => new OrganizationsController.Org { Nx=sel.Nx, Name=Account.GetAuthority(sel.Nx, IsWithUserNameOrShortName:true) }).ToList());
            if (User.IsInRole("Sys.Admin") && IsWithAll && Update)
                Model.Insert(0, new OrganizationsController.Org { Nx = 0, Name = "Все" });
            Session["cbEmployees" + (ViewID == null ? "" : ViewID.Value.ToString())] = Model;
            ViewData["LostFocusEvent" + (ViewID == null ? "" : ViewID.Value.ToString())] = cbEmployees_LostFocus;
            ViewBag.ViewID = ViewID;
            ViewBag.Update = Update;
            ViewBag.IsWithAll = IsWithAll;
            ViewBag.cbOrganizations_LostFocus = cbEmployees_LostFocus;
            ViewBag.Organizations_Nx = Organizations_Nx;
            return PartialView("cbEmployees", Model);
        }
        public ActionResult SaveEmployee(int Profiles_Nx, string FirstName, string SecondName, string ThirdName, int Departments_Nx, int Positions_Nx,string Gender, string Chief, string Email, string Adress, string Passport, string INN, string BirthDate, int? Nx)
        {
            var db = CC.CityConstDB;
            int Organization_Nx = db.Organizations.Single(s => s.Profiles_Nx == Profiles_Nx).Nx;
            int _Profiles_Nx;
            if (!string.IsNullOrEmpty(Chief))
            {
                db.Employees.Where(wh => wh.Organization_Nx == Organization_Nx && wh.Chief == 1).ToList().ForEach(item => { item.Chief = 0; item.DM = DateTime.Now; item.UM = Account.GetCurrentUserID(); });

            }
            if (Nx == null || Nx==0)
            {
                _Profiles_Nx = Account.RegisterNewEmployee(Organization_Nx, Email, FirstName, SecondName, ThirdName, Gender);
                if (_Profiles_Nx > 0)
                {
                    var empl = db.Employees.Single(s => s.Profile_Nx == _Profiles_Nx);
                    empl.Departments_Nx = Departments_Nx;
                    empl.Positions_Nx = Positions_Nx;
                    empl.Profiles.Comments = "Новый пользователь. Данные ввел "+Account.GetAuthority(Account.GetCurrentUserID(),true);
                    empl.State = 1;
                    if (!string.IsNullOrEmpty(Chief))
                        empl.Chief = 1;
                }
            }
            else
            {

                var empl = db.Employees.Single(s => s.Nx == Nx.Value);
                empl.Departments_Nx = Departments_Nx;
                empl.Positions_Nx = Positions_Nx;
                empl.Profiles.FirstName = FirstName;
                empl.Profiles.SecondName = SecondName;
                empl.Profiles.ThirdName = ThirdName;
                empl.Profiles.Gender_Nx = Gender.ToLower() == "male" ? 1 : 2;
                empl.Profiles.Adress = Adress;
                empl.Profiles.Passport = Passport;
                empl.Profiles.INN = INN;
                empl.Profiles.BirthDate =string.IsNullOrEmpty(BirthDate)?null:(DateTime?)DateTime.Parse(BirthDate);
                if (!string.IsNullOrEmpty(Chief))
                    empl.Chief = 1;
            }

          

            db.SaveChanges();
            Account.CheckSystemCache(true);
            return new HttpStatusCodeResult(200);
        }
      
        public ActionResult gvEmployees(int? Profiles_Nx, bool IsReadOnly=false)
        {
            if (Profiles_Nx != null)
            {
                var db = CC.CityConstDB;
                Session["gvEmployees"] = db.Employees.Where(wh => wh.State > 0 && wh.Organizations.Profiles_Nx == Profiles_Nx.Value).OrderByDescending(obd=>obd.Chief).ThenBy(tb=>tb.Profiles.FirstName).ToList().Select(sel => new {sel.Nx,UserState=Account.UsersCache.Single(s=>s.ID==sel.Profiles.Users_ID.Value).State, FIO=Account.GetAuthority(sel.Profile_Nx), Department= sel.Departments!=null?sel.Departments.Name:"", Position= sel.Positions!=null?sel.Positions.Name:"", Contacts= sel.Profiles.Contacts.Count>0?sel.Profiles.Contacts.Where(wh=>wh.State>0).OrderBy(ob=>ob.Priority).Select(s=>s.ContactType.Name+": "+s.Value).Aggregate((x,y)=>x+","+y):"", sel.Chief, UsersName= (sel.Profiles.Users_ID==null?"":Account.UsersCache.Single(s=>s.ID==sel.Profiles.Users_ID).UserName) });
            }

            return View(Session["gvEmployees"]);
        }
        public ActionResult AddEmployee(int? Profiles_Nx, int? Nx, bool IsReadOnly = false)
        {
            var db = CC.CityConstDB;
            if (Profiles_Nx == null && Nx == null)
            {
                var User_ID = Account.GetCurrentUserID();
                var empl = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User_ID)).Employees.Single();
                Profiles_Nx=empl.Organizations.Profiles_Nx;
                Nx = empl.Nx;
            }
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            Models.Employees Model=null;
            if (Nx != null)
                Model = db.Employees.Find(Nx.Value);
            else
            {
                int EmplLimit = db.Organizations.Single(s => s.Profiles_Nx == Profiles_Nx).EmployeesMaxNumber;
                int EmplNum = db.Organizations.Single(s => s.Profiles_Nx == Profiles_Nx).Employees.Where(wh => wh.State > 0).Count();
                if(EmplLimit== EmplNum)
                {
                    return RedirectToAction("Alert", "Home", new { Type=2, Title="Предупреждение", Header="Превышение допустимого кол-ва сотрудников", Message="Сотрудник не может быть добавлен, т.к. достигнуто максимальное кол-во сотрудников для Вашей организации. Обратитесь к Администратору для увелечения максимально допустимого кол-ва сотрудников или удалите уволенных сотрудников.", IsModal =true});
                }
                Model = new Models.Employees { };
            }
            return PartialView(Model);
        }
        public ActionResult CheckUser(string UserName, int Profiles_Nx, int? Nx)
        {
            var db = CC.CityConstDB;
            var usr = Account.GetUser(UserName);
             var usrcheck = usr!=null && (Nx==null || db.Employees.Any(an=>an.State>0 && an.Nx!=Nx.Value && an.Profiles.Users_ID==usr.ID));
            string ret = usrcheck ? "ERROR" : "OK";
            return Content(ret);
        }
        public ActionResult CheckEmployee(string FirstName, string SecondName, string ThirdName, int Profiles_Nx, int? Nx)
        {
            var db = CC.CityConstDB;
            var empl = db.Employees.FirstOrDefault(fod => fod.State > 0 && fod.Organizations.Profiles_Nx == Profiles_Nx && fod.Profiles.FirstName == FirstName && fod.Profiles.SecondName == SecondName && fod.Profiles.ThirdName == ThirdName && (Nx==null || fod.Nx!=Nx.Value));
            string ret = empl == null ? "OK" : "ERROR";
            return Content(ret);
        }
        public ActionResult CheckPassport(string Passport, int? Profiles_Nx)
        {
            Passport = CC.NormalizeName(Passport);
            string ret = Account.ProfilesCache.Any(s => CC.NormalizeName(s.Passport) == Passport && (Profiles_Nx==null || s.Nx!=Profiles_Nx.Value)) ? "ERROR" : "OK";
            return Content(ret);
        }
        public ActionResult DeleteEmployee(int Nx, Guid? curusr = null, DateTime? dttm_n = null, bool ForceCheckSystemCache=true)
        {

            curusr = curusr ?? Account.GetCurrentUserID();
            dttm_n = dttm_n ?? DateTime.Now;
            var db = CC.CityConstDB;
            var sb = CC.SystemModel;
            var empl = db.Employees.Single(s => s.Nx == Nx);
            empl.Profiles.State = 0;
            empl.Profiles.Contacts.ToList().ForEach(item => { item.State = 0;
                item.DM = dttm_n;
                item.UM = curusr;
            });
            var User = sb.Users.Single(s => s.ID == empl.Profiles.Users_ID);
            User.State = 0;
            User.DM = dttm_n;
            User.UM = curusr;
            empl.State = 0;
            empl.DM = dttm_n;
            empl.UM = curusr;
            db.SaveChanges();
            sb.SaveChanges();
            Account.AddSystemLog(17, "Удаление сотрудника, его учетной записи, его профайла и его контактов", User.ID);
            Account.ChangeSecurityStamp(User.ID);
            Account.CheckSystemCache(ForceCheckSystemCache);
            return new HttpStatusCodeResult(200);
        }
        public ActionResult GetUserState(int Nx)
        {
            var db = CC.CityConstDB;
            var User = Account.GetUser(db.Employees.Find(Nx).Profile_Nx);
            return Content(User.State.ToString());
        }
        public ActionResult RestrictonActions(string Reason, int CmdType, int Nx)
        {
            var db = CC.CityConstDB;
            var sb = CC.SystemModel;
            var User =sb.Users.Find(Account.GetUser(db.Employees.Find(Nx).Profile_Nx).Nx);
            switch (CmdType)
            {
                case 1:
                    User.State = 1;
                    User.DM = DateTime.Now;
                    User.UM = Account.GetCurrentUserID();
                    sb.SaveChanges();
                    Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Снятие ограничений на доступ", string.Format("Пользователь {0} снял для Вас ограничения на пользование порталом.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), new List<int> { db.Employees.Find(Nx).Profile_Nx }, DublicateToMail: true);
                    Account.AddSystemLog(2, "Пользователь: " + Account.GetAuthority(User.ID, true) + " Комментарий: " + Reason, User.ID);
                    Account.CheckSystemCache(true);
                    break;
                case 2:
                    var contact = db.Employees.Find(Nx).Profiles.Contacts.Single(s => s.State > 0 && s.Priority== 0);
                    Account.ReIssueToken(contact.Tokens_ID.Value, Reason);
                    break;
                case 3:
                    User.State = 2;
                    User.DM = DateTime.Now;
                    User.UM = Account.GetCurrentUserID();
                    sb.SaveChanges();
                    Account.ChangeSecurityStamp(User.ID);
                    Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Блокировка пользователя", string.Format("Пользователь {0} заблокировал Вас на портале.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), new List<int> { db.Employees.Find(Nx).Profile_Nx }, DublicateToMail: true);
                    Account.AddSystemLog(3, "Пользователь: " + Account.GetAuthority(User.ID, true) + " Комментарий: " + Reason, User.ID);
                    Account.CheckSystemCache(true);
                    break;
                case 4:
                    User.State = 3;
                    User.DM = DateTime.Now;
                    User.UM = Account.GetCurrentUserID();
                    sb.SaveChanges();
                    Account.ChangeSecurityStamp(User.ID);
                    Messages.Mail.SendMessage(Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value, 1, "Ограничение функционала пользователя", string.Format("Пользователь {0} ограничил Вашу работу на портале.<br>Комментарий: {1}<br>Дата: {2}", Account.GetAuthority(Account.GetCurrentUserID().Value), Reason, DateTime.Now.ToString("dd.MM.yyyy HH.mm")), new List<int> { db.Employees.Find(Nx).Profile_Nx }, DublicateToMail: true);
                    Account.AddSystemLog(3, "Пользователь: " + Account.GetAuthority(User.ID, true) + " Комментарий: " + Reason, User.ID);
                    Account.CheckSystemCache(true);
                    break;
            }
            return new HttpStatusCodeResult(200);
        }
    }
}