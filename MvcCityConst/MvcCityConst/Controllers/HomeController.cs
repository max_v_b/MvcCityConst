﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Web.WebSockets;
using System.Net.WebSockets;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;
using System.IO;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var db = CC.CityConstDB;

            ViewBag.Title = "Главная страница";
            var User_ID = Account.GetCurrentUserID();
            var contr = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User_ID)).Employees.Single().Organizations;
            return View(contr);

        }
        public ActionResult WebSocketConnect()
        {
            if (HttpContext.IsWebSocketRequest)
            {
                HttpContext.AcceptWebSocketRequest(Account.ProcessWebsocketSession);
                Account.NotifyAdmins();
            }
           return new HttpStatusCodeResult(101);
        }

        public ActionResult GetFile(string FileName)
        {
            string path = Server.MapPath("/App_Data").TrimEnd('\\')+"\\"+ FileName;
            if (System.IO.File.Exists(path))
            {
                MemoryStream ms=new MemoryStream();
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    //byte[] music;
                    file.CopyTo(ms);
                    ms.Position = 0;
                    var FileToRet = File(ms, "audio/mp3");
                    return FileToRet;
                }
            }
            return new HttpStatusCodeResult(200);
        }
        /// <summary>
        /// Alert for non-logged users
        /// </summary>
        /// <param name="Type">0-Default, 1-Success, 2 - Warning, 3 - Error</param>
        /// <param name="Title"> Page title</param>
        /// <param name="Header">Panel header</param>
        /// <param name="Message">Panel body</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult Alert(int Type=0, string Title=null, string Header=null, string Message=null, bool IsModal=false)
        {
            int ViewID = Math.Abs(DateTime.Now.GetHashCode());
            ViewBag.ViewID = ViewID;
            switch (Type)
            {
                case 0:
                    ViewBag.PanelType = "panel-default";
                    ViewBag.Title = Title ?? "Сообщение";
                    ViewBag.Header = Header ?? "Сообщение";
                    ViewBag.Message = Message ?? "Сообщение";
                    break;
                case 1:
                    ViewBag.PanelType = "panel-success";
                    ViewBag.Title = Title ?? "Успешно";
                    ViewBag.Header = Header ?? "Успешно";
                    ViewBag.Message = Message ?? "Успешно";
                    break;
                case 2:
                    ViewBag.PanelType = "panel-warning";
                    ViewBag.Title = Title ?? "Предупреждение";
                    ViewBag.Header = Header ?? "Предупреждение";
                    ViewBag.Message = Message ?? "Предупреждение";
                    break;
                case 3:
                    ViewBag.PanelType = "panel-danger";
                    ViewBag.Title = Title ?? "Ошибка";
                    ViewBag.Header = Header ?? "Ошибка";
                    ViewBag.Message = Message??"Ошибка";
                    break;
            }
            if (!IsModal)
                return View();
            else
                return PartialView("ModalAlert");
        }

        public ActionResult Comment(string Header, string SuccessAction)
        {
            ViewBag.Header = Header;
            ViewBag.SuccessAction = SuccessAction;
            return PartialView();
        }
      
    }
}