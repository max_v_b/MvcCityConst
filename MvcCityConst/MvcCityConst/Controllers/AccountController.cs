﻿using MvcCityConst.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;


namespace MvcCityConst.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        //[CustomAuthorize(MyRoles:new string[] { "Sys.Admin", "Client.Admin" })]

         //   [Authorize(Roles = "Client.Admin")]
         [Authorize]
        public ActionResult Admin()
        {
          
            return View();
        }
        //[CustomAuthorize(MyRoles: new string[] { "Sys.Admin", "Client.Admin" })]
        [Authorize]
        public ActionResult gvSystemLog()
        {
            var sb = CC.SystemModel;
            var Model = sb.SystemLogs.OrderByDescending(obd => obd.DI).ToList().Select(sel=>new { sel.Nx, sel.DI, sel.SystemEvents_Nx, sel.SystemEvents.Name, sel.TextInfo, UserObject= Account.GetAuthority(sel.User_ID, true), UserInitiate = Account.GetAuthority(sel.UI, true) }).ToList();
            return PartialView(Model);
        }

        public ActionResult gvOnlineUsers()
        {
           var  Clients = CC.WebClients.ToList();
            if (Clients != null && Clients.Count > 0)
            {
                var Users = Account.UsersCache.Where(wh => Clients.Any(any => any.UserID == wh.ID) && wh.State > 0).ToList();
                if (Users != null && Users.Count() > 0)
                {
                    var Profiles = Account.ProfilesCache.Where(wh => Users.Any(any => any.ID == wh.Users_ID) && wh.State > 0).ToList();
                    if (Profiles != null && Profiles.Count() > 0)
                    {
                        
                        var Model= Profiles.Select(sel=> new { sel.Nx, User =Account.GetAuthority(sel.Nx), Org=sel.Employees.Single().Organizations.Name+", "+sel.Employees.Single().Organizations.BusinessLegalForms.Name }).ToList();
                        return PartialView(Model);
                    }
                }
            }
            return PartialView(null);
        }
       [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string ReturnUrl="")
        {
            ViewBag.RedirectUrl = ReturnUrl;
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string UserName, string Password, string ReturnUrl)
        {
            string ControllerName = "Home";
            string ActionName = "Index";
            bool RememberMe = false;
            if (!string.IsNullOrEmpty(Request.Params["RememberMe"]))
                RememberMe = true;
            if (!Account.LogIn(UserName, Password, RememberMe))
            {
                ControllerName = "Home";
                ActionName = "Alert";
                return RedirectToAction(ActionName, ControllerName, new { Type = 3, Title = "Вход в систему - City Construction intranet portal", Header = "Ошибка входа", Message = string.Format("<strong>Внимание!</strong> Вход не выполнен. Попробуйте <a href='{0}'>войти еще раз.</a> Если вы забыли пароль, то пройдите процедуру восстановления пароля. Если вы не зарегистрированны в системе, сделайте запрос на регистрацию в системе.", Url.Action("Login", "Account")) });
            }

            if (true || Account.GetUser(UserName).State == 3)
            {

                if (Account.ShouldToChangePass(UserName))
                    return RedirectToAction("ChangePassword");
            }


            if (!string.IsNullOrEmpty(ReturnUrl))
                return Redirect(ReturnUrl);
            else
                return RedirectToAction(ActionName, ControllerName);


        }
        [AllowAnonymous]
        public ActionResult CreateRegistrationRequest()
        {
            return View();
        }

       [AllowAnonymous]
        public ActionResult ReIssueToken(Guid id)
        {
            var db = CC.CityConstDB;
            var contact = db.Contacts.FirstOrDefault(fod => fod.Tokens_ID == id);
            if (Account.ReIssueToken(id))
            {
                if (contact.ContactType_Nx==1)
                    return RedirectToAction("Alert", "Home", new { Type = 0, Title = "Повторное сообщение на подтверждение", Header = "Повторное сообщение", Message = "Повторное сообщение на подтверждение контакта было отправлено на указанный Вами контакт." });
            }
            return RedirectToAction("Alert", "Home", new {Type=3 });
        }
        public ActionResult GetAuthority()
        {
            //string UserName;
            //var db = CC.CityConstDB;
            ////var sb = CC.SystemModel;
            //Guid UserID = Account.GetCurrentUserID().Value;
            //var profile = db.Profiles.First(f => f.Users_ID == UserID && f.State == 1);
            //UserName = string.IsNullOrEmpty(profile.FirstName) ? User.Identity.Name : string.Format("{0}{1}{2}", profile.FirstName,string.IsNullOrEmpty(profile.SecondName)?"":" "+profile.SecondName.ToUpper()[0]+".", string.IsNullOrEmpty(profile.ThirdName) ? "" : " " + profile.ThirdName.ToUpper()[0] + ".");
            string UserName = Account.GetAuthority(Account.GetCurrentUserID().Value);
            return Content(UserName);
        }
        public ActionResult CheckINN(string INN, int? Profiles_Nx)
        {
            var db = CC.CityConstDB;
            return Content(INN.Length!=12 || Account.ProfilesCache.Any(any=>any.INN== INN && (Profiles_Nx == null || any.Nx != Profiles_Nx.Value)) ?"ERR":"OK");
        }
        [AllowAnonymous]
        public ActionResult Confirmation(Guid id, string Code)
        {
            ViewBag.TokenID = id;
            var Token = ConfirmationTokens.GetAsync(id).Result;
            if (Token != null && ConfirmationTokens.GetState(Token.ID) == ConfirmationTokens.ConfirmationTokenState.Awaiting)
            {
                var IsConfirmed = ConfirmationTokens.ConfirmAsync(id, Code).Result;
                switch (IsConfirmed)
                {
                    case ConfirmationTokens.ConfirmationTokenState.Confirmed:
                        var db = CC.CityConstDB;
                        var sb = CC.SystemModel;
                        var contact = db.Contacts.FirstOrDefault(fod => fod.Tokens_ID == id);
                        if (contact != null)
                        {
                            var UserID = contact.Profiles.Users_ID;
                            contact.State = 1;
                            contact.DM = DateTime.Now;
                            contact.UM = Account.GetCurrentUserID();
                            db.SaveChanges();
                            Account.AddSystemLog(2, "контакт: " + contact.Value);
                            if (UserID != null)
                            {
                                var usr = sb.Users.FirstOrDefault(fod => fod.ID == UserID.Value);
                                if (usr != null && usr.State==2)
                                {
                                    usr.State = 3;
                                    sb.SaveChanges();
                                    Account.AddSystemLog(2, "пользователь: " + usr.UserName);
                                }
                            }
                            Account.CheckSystemCache(true);
                        }
                        return RedirectToAction("Alert", "Home", new { Type = 1, Title = "Подтверждение контакта - City Construction intranet portal", Header = "Контакт успешно подтвержден.", Message = string.Format("<strong>Поздравляем!</strong>Контакт подтвержден. Теперь Вы можете <a href='{0}'>войти в систему.</a>", Url.Action("Login", "Account")) });
                    //break;
                    case ConfirmationTokens.ConfirmationTokenState.Canceled:
                    case ConfirmationTokens.ConfirmationTokenState.Expired:
                        return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Подтверждение контакта - City Construction intranet portal", Header = "Ошибка подтверждения", Message = string.Format("Ссылка больше не действительна. <a href='{0}'>Выслать повторное сообщение для активации</a>", Url.Action("ReIssueToken", "Account", new { id = id })) });
                    //break;
                    case ConfirmationTokens.ConfirmationTokenState.Failed:
                        if (ConfirmationTokens.GetAsync(id).Result != null)
                            return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Подтверждение контакта - City Construction intranet portal", Header = "Ошибка подтверждения", Message = string.Format("Неверный код подтверждения. Осталось попыток: {0}", ConfirmationTokens.GetAsync(id).Result.RemainingTries.ToString()) });
                        else
                            return RedirectToAction("Alert", "Home", new { Type = 3 });
                        // break;
                }
            }
            else
            {
                return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Подтверждение контакта - City Construction intranet portal", Header = "Ошибка подтверждения", Message = string.Format("Ссылка больше не действительна. <a href='{0}'>Выслать повторное сообщение для активации</a>", Url.Action("ReIssueToken", "Account", new { id = id })) });
            }
            return RedirectToAction("Index", "Home");
        }
        [AllowAnonymous]
        public ActionResult PasswordRepareRequest()
        {
            //var Token
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            Account.LogOut();
            return RedirectToAction("Index", "Home");
        }
       
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [Authorize]
        public ActionResult CheckOldPass(string OldPass)
        {
            string ret =Account.VerifyHashedPassword(Account.GetUser(Account.GetCurrentUserID().Value).PasswordHash, OldPass) ? "OK" : "ERROR";
            return Content(ret);
        }
        public ActionResult ChangeOldPassword(string OldPass, string Password, string PasswordConfirm)
        {
           
            if ( Password == PasswordConfirm && Account.ChangeOldPassword(Account.GetCurrentUserID().Value, OldPass, Password))
            {
                Account.AddSystemLog(8, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ErrorMessage = "Пароль не удалось изменить. Неверные учетные данные.";
            return View("Error");
        }
        [AllowAnonymous]
        public ActionResult CheckUserName(string UserName)
        {
            var User = Account.GetUser(UserName);
            string ret = "";
            if (User == null)
                ret = "Пользователь не найден";
            else
                if (User.State == 2)
                ret = "Пользователь заблокирован";
            else
                if (User.State == 0) ret = "Пользователь удален";
            else
            {
                var stateorg = Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == User.ID)).Employees.Single().Organizations.State;
                if (stateorg == 3 && !User.UsersInRoles.Any(any=>any.State==1 && any.Roles.RoleName=="Client.Admin"))
                    ret = "Ограничение для организации. Вход только для Администраторов.";
                else if(stateorg==2)
                    ret = "Ваша организация заблокирована.";
                else if(stateorg == 0)
                    ret = "Нет доступа.";
                else
                ret = "OK";
            }
               
            return Content(ret);
        }
        [AllowAnonymous]
        public ActionResult CheckEmail(string Email,int Contact_Nx=0, int ContactType_Nx=1, bool CheckWithPriority=true)
        {
            var db = CC.CityConstDB;
            bool IsValid = true;
            string ret = "";
            if (ContactType_Nx==1)
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(Email);
                IsValid = regex.Match(Email).Success;
                if (!IsValid)
                    ret = "Неверный формат почты";
            }
             ret=(!CheckWithPriority || !db.Contacts.Any(any=>(any.Nx!=Contact_Nx && any.ContactType_Nx== ContactType_Nx && any.Value.ToLower()==Email.ToLower() && any.State==1 && any.Priority==0))) && IsValid ? "OK" :(!IsValid?ret:"Уже используется");
            return Content(ret);
        }

       [AllowAnonymous]
        public ActionResult RequestReparePassword(string UserName)
        {
            var User = Account.GetUser(UserName);
            string Message = "";
            if (User != null)
            {
                var db = CC.CityConstDB;
                var profile=db.Profiles.FirstOrDefault(fod => fod.Users_ID == User.ID && fod.State==1);
                if (profile != null)
                {
                    var contact = profile.Contacts.FirstOrDefault(fod => fod.ContactType_Nx == 1 && fod.Priority == 0);
                    if (contact != null)
                    {
                        var Token = ConfirmationTokens.Issue("Восстановление пароля", 8);
                        contact.Tokens_ID = Token.ID;
                        contact.DM = DateTime.Now;
                        contact.UM = Account.GetCurrentUserID();
                        db.SaveChanges();
                        string UserPassword = User.UserName.Split('@')[0] + User.DI.ToString("yyyyMMddHHmmss");
                        string Body = string.Format("Для восстановления пароля, Вам необходимо перейти по <a href='{0}'>данной ссылке</a>, после этого Ваш пароль станет:{1}<br/>Затем Вам будет необходимо сменить пароль.","http://"+Request.Url.Authority+Url.Action("PasswordRepare", "Account", new { id = Token.ID, Code = Token.Code }), UserPassword);
                        Mail.SendMail(contact.Value, "Восстановления пароля", Body);
                        return RedirectToAction("Alert", "Home", new { Type = 2, Title = "Запрос на восстановления пароля принят", Header = "Запрос на восстановления пароля", Message = "На Вашу почту отправлено письмо с инструкциями по восстановленю пароля." });
                    }
                    else
                        Message = "В системе нет Ваших контактных данных. Обратитесь к Администратору системы.";
                }
                else
                    Message = "Непредвиденная ошибка. Обратитесь к Администратору системы.";
            }
            else
                Message = "Пользователь не найден в системе. Обратитесь к Администратору системы.";
            return RedirectToAction("Alert", "Home",new { Type=3, Message = Message });
        }

        [AllowAnonymous]
        public ActionResult PasswordRepare(Guid id, string Code)
        {
            ViewBag.TokenID = id;
            var Token = ConfirmationTokens.GetAsync(id).Result;
            if (Token != null && ConfirmationTokens.GetState(Token.ID) == ConfirmationTokens.ConfirmationTokenState.Awaiting)
            {
                var IsConfirmed = ConfirmationTokens.ConfirmAsync(id, Code).Result;
                if (IsConfirmed == ConfirmationTokens.ConfirmationTokenState.Confirmed)
                {

                    var db = CC.CityConstDB;
                    var sb = CC.SystemModel;
                    var contact = db.Contacts.FirstOrDefault(fod => fod.Tokens_ID == id);
                    if (contact != null)
                    {
                        var UserID = contact.Profiles.Users_ID;

                        if (UserID != null)
                        {
                            var usr = sb.Users.FirstOrDefault(fod => fod.ID == UserID.Value);
                            if (usr != null)
                            {
                                string UserPassword = usr.UserName.Split('@')[0] + usr.DI.ToString("yyyyMMddHHmmss");
                                if (usr.State == 2)
                                {
                                    usr.State = 1;
                                    usr.DM = DateTime.Now;
                                    usr.UM = Account.GetCurrentUserID();
                                    Account.AddSystemLog(2, usr.UserName);
                                }
                                usr.PasswordHash = Account.HashPassword(UserPassword);
                                sb.SaveChanges();
                                Account.CheckSystemCache(true);
                                Account.ChangeSecurityStamp(usr.ID);
                                Account.AddSystemLog(8, usr.UserName);
                            }
                        }
                    }
                    return RedirectToAction("Alert", "Home", new { Type = 1, Title = "Пароль сброшен - City Construction intranet portal", Header = "Пароль успешно сброшен.", Message = string.Format("<strong>Внимание!</strong>Ваш старый пароль сброшен в соответствии с полученными Вами по почте инструкциями. Теперь Вы можете <a href='{0}'>войти в систему.</a>", Url.Action("Login", "Account")) });
                }
            }
            return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Ошибочная ссылка - City Construction intranet portal", Header = "Ошибочная ссылка", Message = string.Format("Ссылка больше не действительна.")});
        }

       
       [AllowAnonymous]
        public ActionResult RegistrationRequest(int? cbOrganizationsValue, string Email, string FirstName, string SecondName, string ThirdName,string Gender, string OrgName, int? cbBusinessLegalFormsValue, string OrgEmail)
        {
            if (cbOrganizationsValue != null)
            {
                if(Account.RegisterNewEmployee(cbOrganizationsValue.Value, Email, FirstName, SecondName, ThirdName, Gender)>0)
                {
                    return RedirectToAction("Alert", "Home", new { Type = 1, Title = "Успешная регистрация - City Construction intranet portal", Header = "Успешная регистрация", Message = string.Format("<strong>Внимание!</strong>Регистрация прошла успешно. На Ваш адрес было отправлено письмо с инструкциями по активации Вашего аккаунта.") });
                }
                else
                {
                    return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Ошибка регистрации - City Construction intranet portal", Header = "Ошибка регистрации", Message = string.Format("<strong>Внимание!</strong>Вас не удалось зарегистрировать в системе. Возможно, вы указали не свой Email и/или не указали свою фамилию, и/или Ваша организация не зарегистрированна в системе. Попробуйте пройти повторную регистрацию.") });
                }
            }
            else
            {
                if(Account.RegisterNewOrganization(OrgName, cbBusinessLegalFormsValue.Value, OrgEmail)>0)
                {
                    return RedirectToAction("Alert", "Home", new { Type = 1, Title = "Успешная регистрация - City Construction intranet portal", Header = "Успешная регистрация", Message = string.Format("<strong>Внимание!</strong>Регистрация прошла успешно. На Ваш адрес было отправлено письмо с инструкциями по активации Вашего аккаунта.") });
                }
                else
                {
                    return RedirectToAction("Alert", "Home", new { Type = 3, Title = "Ошибка регистрации - City Construction intranet portal", Header = "Ошибка регистрации", Message = string.Format("<strong>Внимание!</strong>Вас не удалось зарегистрировать в системе. Возможно, вы указали не свой Email  и/или Ваша организация уже зарегистрированна в системе. Попробуйте пройти повторную регистрацию.") });
                }
            }
        }
    }
}