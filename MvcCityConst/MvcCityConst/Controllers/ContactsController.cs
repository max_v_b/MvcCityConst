﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    public class ContactsController : Controller
    {
        public ActionResult ContactsControl(int Profiles_Nx, bool IsReadOnly)
        {
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView();
        }

        public ActionResult gvContacts(int? Profiles_Nx, bool IsReadOnly = true)
        {
            if (Profiles_Nx != null)
            {
                var db = CC.CityConstDB;
                var Model = db.Contacts.Where(wh => wh.Profiles_Nx == Profiles_Nx.Value && wh.State != 0).Select(sel => new { sel.Nx, sel.ID, sel.Priority, sel.State, sel.ContactType_Nx, sel.ContactType.Name, sel.Value, sel.Comments }).OrderBy(ob => ob.Priority).ToList();
                Session["gvContacts"] = Model;
                Session["gvContacts_IsReadOnly"] = IsReadOnly;
            }
            ViewBag.IsReadOnly = (bool)Session["gvContacts_IsReadOnly"];
            return PartialView(Session["gvContacts"]);
        }
        public ActionResult EditContact(int Profiles_Nx, int? Nx = null, bool IsReadOnly = true)
        {
            var db = CC.CityConstDB;
            Models.Contacts Model = new Models.Contacts { Priority = 1 };
            if (Nx != null)
                Model = db.Contacts.Find(Nx.Value);
            ViewBag.Profile_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Model);
        }
        public ActionResult DeleteContact(int Contacts_Nx)
        {
            var db = CC.CityConstDB;
            var cnt = db.Contacts.Single(s => s.Nx == Contacts_Nx);
            cnt.State = 0;
            cnt.DM = DateTime.Now;
            cnt.UM = Account.GetCurrentUserID();
            db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }
        public ActionResult cbContactType(string InputValueName, int? ContactTypeNx, bool IsReadOnly = false)
        {
            ViewBag.InputValueName = InputValueName;
            ViewBag.ContactTypeNx = ContactTypeNx;
            ViewBag.IsReadOnly = IsReadOnly;
            var db = CC.CityConstDB;
            var Model = db.ContactType.ToList();
            return PartialView(Model);
        }

        public ActionResult CheckPriorityContact(int Profiles_Nx)
        {
            return Content(CC.CityConstDB.Contacts.Any(any => any.Profiles_Nx == Profiles_Nx && any.State > 0 && any.Priority == 0) ? "OK" : "ERROR");
        }
        // { Profiles_Nx:'@ViewBag.Profile_Nx',Contact_Nx:$('#Contact_Nx').val() , Value: $('#Value').val(), ContactType_Nx: cbContactType.GetValue(), Priority: !!$('#Priority').attr('checked') }
        public ActionResult SaveContact(int Profiles_Nx, int? Contact_Nx, string Value, int ContactType_Nx, bool Priority, string Comments)
        {
            var db = CC.CityConstDB;
            Models.Contacts Contact;
            Models.Tokens Token = null;
            if (Priority)
                db.Contacts.Where(wh => wh.Profiles_Nx == Profiles_Nx).ToList().ForEach(cnt => db.Contacts.Single(s => s.Nx == cnt.Nx).Priority = 1);
            if (Contact_Nx == null || Contact_Nx == 0)
            {
                Contact = new Models.Contacts();
                Contact.Comments = Comments;
                Contact.ContactType_Nx = ContactType_Nx;
                Contact.ID = Guid.NewGuid();
                Contact.Profiles_Nx = Profiles_Nx;
                Contact.Priority = Priority ? 0 : 1;
                Contact.State = 3;
                Token = ConfirmationTokens.Issue("Подтверждение контакта", 12);
                Contact.Tokens_ID = Token.ID;

                Contact.DI = DateTime.Now;
                Contact.UI = Account.GetCurrentUserID();
                Contact.Value = Value;
                db.Contacts.Add(Contact);


            }
            else
            {
                Contact = db.Contacts.Single(s => s.Nx == Contact_Nx.Value);
                Contact.Comments = Comments;
                Contact.ContactType_Nx = ContactType_Nx;
                Contact.Priority = Priority ? 0 : 1;
                if (Contact.Value != Value)
                {
                    Contact.State = 3;
                    Token = ConfirmationTokens.Issue("Подтверждение контакта", 12);
                    Contact.Tokens_ID = Token.ID;
                }
                Contact.DM = DateTime.Now;
                Contact.UM = Account.GetCurrentUserID();
                Contact.Value = Value;
            }
            db.SaveChanges();
            if (ContactType_Nx == 1 && Token != null)
            {
                string href = string.Format("http://{0}/Account/Confirmation/{1}?Code={2}", HttpContext.Request.Url.Authority, Token.ID.ToString(), Token.Code.ToString());
                string Body = string.Format("Указанный Вами контакт будет разблокирован после подтверждения. Для подтверждения и активации контакта перейдите по <a href='{0}'>ссылке</a>", href);
                Messages.Mail.SendMail(Contact.Value, "Подтверждение контакта", Body);
                Messages.Mail.SendMessage(Account.ProfilesCache.Single(s=>s.ID== Account.SystemProfileID).Nx, 2, "Подтверждения контакта", "На указанный Вами контакт отправлено письмо с инструкциями по подтверждению контакта", new List<int> { Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value).Value });
            }
            return new HttpStatusCodeResult(200);
        }

    }
}