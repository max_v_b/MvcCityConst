﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    [Authorize]
    public class JobsController : Controller
    {
        // GET: Jobs
        public ActionResult Index()
        {
           // var ds = CC.CityConstModel;
           //// bool IsUserBinded = ds.СпСотрСпРаботы.Any(any => any.СпСотрудники.SysUser == User.Identity.Name && any.СпСотрудники.State == 1);
           // int? EmplID = null;
            
           //     EmplID = ds.СпСотрСпРаботы.First(f => f.СпСотрудники.SysUser == User.Identity.Name && f.СпСотрудники.State == 1).IDСпСотрудники;
           // ViewBag.Contragent = ds.СпКонтрагенты.First(f => f.IDКонтрагенты == CC.ContrID);
           // ViewBag.EmplID = EmplID;

            var db = CC.CityConstDB;
            var CurUsr = Account.GetCurrentUserID();
            bool IsUserBinded = db.JobsToEmployees.Any(any => any.Employees.Profiles.Users_ID == CurUsr && any.Employees.State > 0 && any.State > 0);
            int? Nx = null;
            if (IsUserBinded)
                Nx = Account.ProfilesCache.Single(s => s.Nx == Account.GetUserProfile_Nx(Account.GetCurrentUserID().Value)).Employees.FirstOrDefault().Nx;
           
            ViewBag.Org= Account.ProfilesCache.FirstOrDefault(fod => fod.Employees.Any(s => s.State > 0 && s.Profiles.Users_ID == CurUsr)).Employees.Single().Organizations;
            ViewBag.Nx = Nx;
            return View();

        }
        public ActionResult BindToEmployee(int? Nx, string SelectedJobs, int FormType)
        {
                    if (!string.IsNullOrEmpty(SelectedJobs))
                    {
                        SelectedJobs = SelectedJobs.TrimStart('/');
                       // var ds = CC.CityConstModel;
                var db = CC.CityConstDB;
                if(Nx!=null)
                Nx = db.Profiles.Single(s => s.Nx == Nx.Value).Employees.First().Nx;
                var User_ID = Account.GetCurrentUserID();
                        SelectedJobs.Split('/').ToList().ForEach(item =>
                        {
                            int i = int.Parse(item);
                            switch (FormType)
                            {
                                case 0:
                                case 1:

                                    if ( !db.JobsToEmployees.Any(any=>any.State >0 && any.JobsAppointed_Nx==i && any.Employees_Nx==Nx) /*!ds.СпСотрСпРаботы.Any(any => any.IDРаботы == i && any.IDСпСотрудники == IDСотрудники)*/)
                                    {
                                        if (FormType == 1)
                                            db.JobsToEmployees.Where(wh => wh.JobsAppointed_Nx == i).ToList().ForEach(fe => { fe.State = 0; fe.DM = DateTime.Now; fe.UM = User_ID; });
                                        // ds.СпСотрСпРаботы.RemoveRange(ds.СпСотрСпРаботы.Where(wh => wh.IDРаботы == i));
                                        //ds.СпСотрСпРаботы.Add(new Models.СпСотрСпРаботы { IDСпСотрудники = IDСотрудники, IDРаботы = i });
                                        db.JobsToEmployees.Add(new Models.JobsToEmployees {  Employees_Nx=Nx.Value, JobsAppointed_Nx=i, DI=DateTime.Now, UI= User_ID, State=1 });
                                    }
                                    break;
                                case 2:
                                    //ds.СпСотрСпРаботы.RemoveRange(ds.СпСотрСпРаботы.Where(wh => wh.IDРаботы == i && wh.IDСпСотрудники== IDСотрудники.Value));
                                    db.JobsToEmployees.Where(wh => wh.JobsAppointed_Nx == i && wh.Employees_Nx == Nx.Value).ToList().ForEach(fe => { fe.State = 0; fe.DM = DateTime.Now; fe.UM = User_ID; });
                                    break;
                                case 3:
                                    //ds.СпСотрСпРаботы.RemoveRange(ds.СпСотрСпРаботы.Where(wh => wh.IDРаботы == i));
                                    db.JobsToEmployees.Where(wh => wh.JobsAppointed_Nx == i).ToList().ForEach(fe => { fe.State = 0; fe.DM = DateTime.Now; fe.UM = User_ID; });
                                    break;
                            }
                        });
                        db.SaveChanges();
            }
            
           
            return new HttpStatusCodeResult(200);
        }
       // gvJobs", new { Profiles_Nx = ViewBag.Org.Profiles_Nx, Nx= ViewBag.Nx })
        public ActionResult gvJobs(int? Profiles_Nx , int? Nx)
        {
            var ds = CC.CityConstModel;
          // int? ID = CC.ContrID.Value;
            var db = CC.CityConstDB;
            bool IsInRole = User.IsInRole("Client.Admin") || User.IsInRole("Sys.Admin");
            if (Session["gvJobs"] == null || Profiles_Nx != null)
            {
                var Model = db.JobsAppointed.Where(wh =>wh.State>0 && wh.Organizations.Profiles_Nx == Profiles_Nx &&(IsInRole || Nx == null || wh.JobsToEmployees.Any(any => any.Employees_Nx == Nx))).Select(sel => new
                {
                    sel.Nx,
                    JobsName = sel.Jobs.Name,
                    IsBinded = sel.JobsToEmployees.Count(),
                    BindetToEmpl = sel.JobsToEmployees.Where(wh=>wh.State>0).Select(s => s.Employees.Profile_Nx),
                    ManagedObjectLocationName = sel.ManagedObjectLocation.Name,
                    sel.PlanedDateBeg,
                    sel.PlanedDateEnd,
                    sel.Comments,
                    sel.DeclaredDateBeg,
                    sel.DeclaredDateEnd,
                    ResourceDeclared = db.JobsExecutedDeclared.Where(wh =>wh.State>0 && wh.JobsAppointedWithResources.JobsAppointed.Nx== sel.Nx).Count(), // ds.РаботаФактПодрядчик.Where(wh => wh.РаботаРесурсОбъем.Работы.IDРаботы == sel.IDРаботы).Count(),
                    ResourceApproved = db.JobsExecutedDeclared.Where(wh => wh.State > 0 && wh.JobsAppointedWithResources.JobsAppointed.Nx == sel.Nx && wh.VolumeConfirmed != null).Count() // ds.РаботаФактПодрядчик.Where(wh => wh.РаботаРесурсОбъем.Работы.IDРаботы == sel.IDРаботы && wh.РаботаФакт != null).Count()
                }).OrderBy(ob => ob.JobsName).ToList().Select(sel => new
                {
                    BindetToEmpl = sel.BindetToEmpl.Count() > 0 ? sel.BindetToEmpl.Select(s => Account.GetAuthority(s)).Aggregate((x, y) => x + ";" + y) : "не распределена",
                    sel.Nx,
                    sel.IsBinded,
                    sel.ResourceApproved,
                    sel.ResourceDeclared,
                    sel.JobsName,
                    sel.PlanedDateBeg,
                    sel.PlanedDateEnd,
                    sel.Comments,
                    sel.ManagedObjectLocationName,
                    sel.DeclaredDateBeg,
                    sel.DeclaredDateEnd
                }).ToList();
                Session["gvJobs"] = Model;
            }
            ViewBag.FullJobList = Nx == null || IsInRole;
            ViewBag.Profiles_Nx = Profiles_Nx;
            return View(Session["gvJobs"]);
        }
    }
}
