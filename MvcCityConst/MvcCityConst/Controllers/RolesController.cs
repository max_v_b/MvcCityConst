﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CC = MvcCityConst.Common;

namespace MvcCityConst.Controllers
{
    public class RolesController : Controller
    {
        public ActionResult UserInRoles(int Profiles_Nx, bool IsReadOnly=false)
        {
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView();
        }
        //gvUserInRoles", new { Profiles_Nx
        public ActionResult gvUserInRoles(int? Profiles_Nx, bool IsReadOnly = false)
        {
            var sb = CC.SystemModel;
         
            if (Session["gvUserInRoles"] == null || Profiles_Nx != null)
            {
                var Usr_ID = Account.ProfilesCache.Single(s => s.Nx == Profiles_Nx).Users_ID;
                var Model = sb.UsersInRoles.Where(wh => wh.State > 0 && wh.Users.ID==Usr_ID).Select(sel => new { sel.Nx, sel.Roles.RoleName }).ToList();
                Session["gvUserInRoles"] = Model;
            }
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.IsReadOnly = IsReadOnly;
            return PartialView(Session["gvUserInRoles"]);
        }
        //AddRole',{Profiles_Nx:'@ViewBag.Profiles_Nx'},
        public ActionResult AddRole(int Profiles_Nx)
        {
            ViewBag.Profiles_Nx = Profiles_Nx;
            return PartialView();
        }
        // cbRoles", new { Profiles_Nx
        public ActionResult cbRoles(int Profiles_Nx, string InputValueName)
        {
            var sb = CC.SystemModel;
            var Usr_ID = Account.ProfilesCache.Single(s => s.Nx == Profiles_Nx).Users_ID;
            ViewBag.Profiles_Nx = Profiles_Nx;
            ViewBag.InputValueName = InputValueName;
            bool IsSysAdmin = User.IsInRole("Sys.Admin");
            
            var Model = sb.Roles.Where(wh => !wh.UsersInRoles.Any(any => any.Users.ID == Usr_ID && any.State > 0) && (IsSysAdmin || wh.RoleName!="Sys.Admin")).ToList();
            return PartialView(Model);
        }
        //AddNewRole cbRolesValue
        public ActionResult AddNewRole(int Profiles_Nx, int cbRolesValue/*, params string[] Params*/)
        {
            var sb = CC.SystemModel;
            var Usr_ID = Account.ProfilesCache.Single(s => s.Nx == Profiles_Nx).Users_ID;
            var Usr = sb.Users.Single(s => s.ID == Usr_ID);
            Usr.UsersInRoles.Add(new Models.UsersInRoles { Users_Nx = Usr.Nx, Roles_Nx = cbRolesValue, State = 1 });
            Usr.DM = DateTime.Now;
            Usr.UM = Account.GetCurrentUserID();
            sb.SaveChanges();
            Account.AddSystemLog(4, sb.Roles.Single(s => s.Nx == cbRolesValue).RoleName, Usr.ID);
            Account.CheckSystemCache(true);
            Account.ChangeSecurityStamp(Usr_ID.Value);
            return new HttpStatusCodeResult(200);
        }
        //DeleteRole', { Nx:
        public ActionResult DeleteRole(int Nx)
        {
            var sb = CC.SystemModel;
            var uir = sb.UsersInRoles.Find(Nx);
            uir.State = 0;
            uir.Users.DM = DateTime.Now;
            uir.Users.UM = Account.GetCurrentUserID();
            sb.SaveChanges();
            Account.CheckSystemCache(true);
            Account.ChangeSecurityStamp(uir.Users.ID);
            Account.AddSystemLog(5,uir.Roles.RoleName,uir.Users.ID);
            return new HttpStatusCodeResult(200);
        }
    }
}